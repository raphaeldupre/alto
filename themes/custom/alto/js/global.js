(function ($, Drupal) {

  'use strict';

  // Active ou désactive les liens principaux selon la page courante et cache les lignes inutiles de menu
  if ($('body').hasClass('page-node-2') || $('body').hasClass('page-node-3') || $('body').hasClass('page-node-4') || $('body').hasClass('page-node-5') || $('body').hasClass('page-node-6') || $('body').hasClass('page-node-7')) {
    $('.custom-nav-top .custom-nav-comprendre').addClass('active');
    $('.custom-nav-bottom .custom-nav-bottom-comprendre').addClass('active');
    $('.custom-nav-bottom .custom-nav-bottom-prise-en-charge').addClass('hide');
    $('.custom-nav-bottom .custom-nav-bottom-suivi').addClass('hide');
  } else if ($('body').hasClass('page-node-8') || $('body').hasClass('page-node-9') || $('body').hasClass('page-node-10') || $('body').hasClass('page-node-11') || $('body').hasClass('page-node-12') || $('body').hasClass('page-node-13')) {
    $('.custom-nav-top .custom-nav-pec').addClass('active');
    $('.custom-nav-bottom .custom-nav-bottom-comprendre').addClass('hide');
    $('.custom-nav-bottom .custom-nav-bottom-prise-en-charge').addClass('active');
    $('.custom-nav-bottom .custom-nav-bottom-suivi').addClass('hide');
  } else if ($('body').hasClass('page-node-14') || $('body').hasClass('page-node-15') || $('body').hasClass('page-node-16') || $('body').hasClass('page-node-17') || $('body').hasClass('page-node-18') || $('body').hasClass('page-node-19')) {
    $('.custom-nav-top .custom-nav-suivi').addClass('active');
    $('.custom-nav-bottom .custom-nav-bottom-comprendre').addClass('hide');
    $('.custom-nav-bottom .custom-nav-bottom-prise-en-charge').addClass('hide');
    $('.custom-nav-bottom .custom-nav-bottom-suivi').addClass('active');
  } else {
    $('.custom-nav-bottom .custom-nav-bottom-comprendre').addClass('active');
    $('.custom-nav-bottom .custom-nav-bottom-prise-en-charge').addClass('hide');
    $('.custom-nav-bottom .custom-nav-bottom-suivi').addClass('hide');
  }

  // Active les liens secondaires si la page est active
  if ($('body').hasClass('page-node-2')) {
    $('.custom-nav-bottom-comprendre .arythmie-cardiaque').addClass('current');
  } else if ($('body').hasClass('page-node-3')) {
    $('.custom-nav-bottom-comprendre .avc').addClass('current');
  } else if ($('body').hasClass('page-node-4')) {
    $('.custom-nav-bottom-comprendre .phlebite').addClass('current');
  } else if ($('body').hasClass('page-node-5')) {
    $('.custom-nav-bottom-comprendre .embolie-pulmonaire').addClass('current');
  } else if ($('body').hasClass('page-node-6')) {
    $('.custom-nav-bottom-comprendre .maladie-coronaire').addClass('current');
  } else if ($('body').hasClass('page-node-7')) {
    $('.custom-nav-bottom-comprendre .arterite-des-membres-inferieurs').addClass('current');
  } else if ($('body').hasClass('page-node-8')) {
    $('.custom-nav-bottom-prise-en-charge .arythmie-cardiaque').addClass('current');
  } else if ($('body').hasClass('page-node-9')) {
    $('.custom-nav-bottom-prise-en-charge .avc').addClass('current');
  } else if ($('body').hasClass('page-node-10')) {
    $('.custom-nav-bottom-prise-en-charge .phlebite').addClass('current');
  } else if ($('body').hasClass('page-node-11')) {
    $('.custom-nav-bottom-prise-en-charge .embolie-pulmonaire').addClass('current');
  } else if ($('body').hasClass('page-node-12')) {
    $('.custom-nav-bottom-prise-en-charge .maladie-coronaire').addClass('current');
  } else if ($('body').hasClass('page-node-13')) {
    $('.custom-nav-bottom-prise-en-charge .arterite-des-membres-inferieurs').addClass('current');
  } else if ($('body').hasClass('page-node-14')) {
    $('.custom-nav-bottom-suivi .arythmie-cardiaque').addClass('current');
  } else if ($('body').hasClass('page-node-15')) {
    $('.custom-nav-bottom-suivi .avc').addClass('current');
  } else if ($('body').hasClass('page-node-16')) {
    $('.custom-nav-bottom-suivi .phlebite').addClass('current');
  } else if ($('body').hasClass('page-node-17')) {
    $('.custom-nav-bottom-suivi .embolie-pulmonaire').addClass('current');
  } else if ($('body').hasClass('page-node-18')) {
    $('.custom-nav-bottom-suivi .maladie-coronaire').addClass('current');
  } else if ($('body').hasClass('page-node-19')) {
    $('.custom-nav-bottom-suivi .arterite-des-membres-inferieurs').addClass('current');
  }

  // Ajoute 9 colonnes au fil d'arianne
  $('.breadcrumb').addClass('col-lg-8');
  $('.breadcrumb').addClass('col-md-12');

  // Show more des pages custom
  $('.view-telechargements-et-liens').showMore({
    minheight: 240,
    buttontxtmore: 'Voir la suite',
    buttontxtless: 'Voir moins',
    animationspeed: 250
  });      

  // Modifications pour les tailles d'écrans LG et XL
  if ($(window).width() > 975) {

    // Fixe la barre de navigation une fois arrivé au bas du header
    function custom_sticky_nav() {

      var window_top = $(window).scrollTop();
      var div_top = $('.wrapper-custom-nav').offset().top;

      if (window_top > div_top) {
        $('.wrapper-custom-nav').addClass('custom-sticky-nav');
        $('.main-content').addClass('top-sticky-nav-space');
      }

      if(window_top < 890) {
        $('.wrapper-custom-nav').removeClass('custom-sticky-nav');
        $('.main-content').removeClass('top-sticky-nav-space');
      }

    }
    $(window).scroll(custom_sticky_nav);

  }


  // Vitesse de scroll au chargement de page et smooth scroll
  if (window.location.hash) {
    var hash = window.location.hash;
    if ($(hash).length) {
        $('html, body').animate({
            scrollTop: $(hash).offset().top -415
        }, 3000, 'swing');
    }
  }
  // Scroll des ancres du menu de droite
  $(document).on('click', 'a[href^="#"]', function (event) {
    event.preventDefault();

    $('html, body').animate({
        scrollTop: $($.attr(this, 'href')).offset().top -170
    }, 500);
  });
  // Scroll du Quizz
  $(document).on('click', 'a[href="#quizz-fa"]', function (event) {
    event.preventDefault();

    $('html, body').animate({
        scrollTop: $($.attr(this, 'href')).offset().top -220
    }, 500);
  });

  
})(jQuery, Drupal);