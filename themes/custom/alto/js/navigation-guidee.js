(function ($, Drupal) {

    'use strict';
  
    /* Gestion de la navigation guidée */
    // État initial de tous les éléments de formulaire
    $('.modal-body-entree').show();
    $('.modal-body-pathologie').hide();
    $('.modal-body-parcours-info-full').hide();
    $('.modal-body-parcours-info-min').hide();
    $('.modal-body-comprendre-first').hide();
    $('.modal-body-comprendre-sec').hide();
    $('.modal-body-comprendre-third').hide();
    $('.modal-body-pec-first-full').hide();
    $('.modal-body-pec-first-avc').hide();
    $('.modal-body-pec-sec-avc').hide();
    $('.modal-body-pec-first-mc').hide();
    $('.modal-body-suivi-first').hide();


    // Réinitialisation après fermeture de la popup
    $('.modal-header .close').click(function(){
        $('.modal-body-entree').show();
        $('.modal-body-pathologie').hide();
        $('.modal-body-parcours-info-full').hide();
        $('.modal-body-parcours-info-min').hide();
        $('.modal-body-comprendre-first').hide();
        $('.modal-body-comprendre-sec').hide();
        $('.modal-body-comprendre-third').hide();
        $('.modal-body-pec-first-full').hide();
        $('.modal-body-pec-first-avc').hide();
        $('.modal-body-pec-sec-avc').hide();
        $('.modal-body-pec-first-mc').hide();
        $('.modal-body-suivi-first').hide();
        $('#modal-body-pathologie-ami').removeClass('pathologie-active');
        $('#modal-body-pathologie-mc').removeClass('pathologie-active');
        $('#modal-body-pathologie-ac').removeClass('pathologie-active');
        $('#modal-body-pathologie-avc').removeClass('pathologie-active');
        $('#modal-body-pathologie-phlebite').removeClass('pathologie-active');
        $('#modal-body-pathologie-ep').removeClass('pathologie-active');
    });


    // Liens Comprendre > Causes
    var lienCausesArythmie = "/comprendre/arythmie-cardiaque#causes-et-facteurs-de-risque";
    var lienCausesAVC = "/comprendre/avc#causes-et-facteurs-de-risque";
    var lienCausesPhlebite = "/comprendre/phlebite#causes-et-facteurs-de-risque";
    var lienCausesEmbolie = "/comprendre/embolie-pulmonaire#causes-et-facteurs-de-risque";
    var lienCausesMaladieC = "/comprendre/maladie-coronaire#introduction";
    var lienCausesArterite = "/comprendre/arterite-des-membres-inferieurs#introduction";

    // Liens Comprendre > Diagnostic
    var lienDiagnosticArythmie = "/comprendre/arythmie-cardiaque#diagnostic";
    var lienDiagnosticPhlebite = "/comprendre/phlebite#diagnostic";
    var lienDiagnosticEmbolie = "/comprendre/embolie-pulmonaire#diagnostic";

    // Liens Comprendre > Symptômes
    var lienSymptomesArythmie = "/comprendre/arythmie-cardiaque#symptomes";
    var lienSymptomesAVC = "/comprendre/avc#signes-d-alerte---vite";
    var lienSymptomesPhlebite = "/comprendre/phlebite#symptomes";
    var lienSymptomesEmbolie = "/comprendre/embolie-pulmonaire#symptomes";
    var lienSymptomesMaladieC = "/comprendre/maladie-coronaire#symptomes";
    var lienSymptomesArterite = "/comprendre/arterite-des-membres-inferieurs#symptomes";

    // Liens Comprendre > Complications
    var lienCompliquationsArythmie = "/comprendre/arythmie-cardiaque#complications";
    var lienCompliquationsAVC = "/comprendre/avc#consequences-et-complications-de-l-avc";
    var lienCompliquationsPhlebite = "/comprendre/phlebite#complication-de-la-phlebite";
    var lienCompliquationsEmbolie = "/comprendre/embolie-pulmonaire#complication-de-l-embolie-pulmonaire";
    var lienCompliquationsMaladieC = "/comprendre/maladie-coronaire#evolution-et-complications";
    var lienCompliquationsArterite = "/comprendre/arterite-des-membres-inferieurs#evolution";

    // Liens Prise en charge > Différents traitements
    var lienDiffTraitArythmie = "/prise-en-charge/arythmie-cardiaque#les-differentes-prises-en-charge";
    var lienDiffTraitPhlebite = "/prise-en-charge/phlebite#la-prise-en-charge-de-la-phlebite";
    var lienDiffTraitEmbolie = "/prise-en-charge/embolie-pulmonaire#la-prise-en-charge-de-l-embolie-pulmonaire";

    // Liens Prise en charge > Anticoagulants
    var lienPecAnticoagulantsArythmie = "/prise-en-charge/arythmie-cardiaque#les-anticoagulants-dans-la-fa";
    var lienPecAnticoagulantsPhlebite = "/prise-en-charge/phlebite#les-anticoagulants";
    var lienPecAnticoagulantsEmbolie = "/prise-en-charge/embolie-pulmonaire#les-anticoagulants";

    // Liens Prise en charge > Démarches AVC
    var lienDemarchesAVC = "/prise-en-charge/avc#les-demarches-medico-sociales";

    // Liens Prise en charge > Après un AVC
    var lienApresAVC = "/prise-en-charge/avc#la-prise-en-charge-post-avc";

    // Liens Prise en charge > Prévention AVC 
    var lienPreventionAVC = "/prise-en-charge/avc#la-prevention";

    // Liens Prise en charge > Modification du mode de vie Maladie Coronaire / Artérite
    var lienModifModeMaladieC = "/prise-en-charge/maladie-coronaire#modification-du-mode-de-vie";
    var lienModifModeArterite = "/prise-en-charge/arterite-des-membres-inferieurs#modification-du-mode-de-vie";

    // Liens Prise en charge > Prise en charge médicale Maladie Coronaire / Artérite
    var lienPecMedMaladieC = "/prise-en-charge/maladie-coronaire#prise-en-charge-interventionnelle-et-chirurgicale";
    var lienPecMedArterite = "/prise-en-charge/arterite-des-membres-inferieurs#prise-en-charge-interventionnelle-et-chirurgicale";

    // Liens Suivi > Associations
    var lienAssociationsArythmie = "/suivi/arythmie-cardiaque#les-associations";
    var lienAssociationsAVC = "/suivi/avc#les-associations";
    var lienAssociationsPhlebite = "/suivi/phlebite#les-associations";
    var lienAssociationsEmbolie = "/suivi/embolie-pulmonaire#les-associations";

    // Liens Suivi > Anticoagulants
    var lienSuiviAnticoagulantsArythmie = "/suivi/arythmie-cardiaque#bien-vivre-avec-un-traitement-anticoagulant";
    var lienSuiviAnticoagulantsAVC = "/suivi/avc#bien-vivre-avec-un-traitement-anticoagulant";
    var lienSuiviAnticoagulantsPhlebite = "/suivi/phlebite#bien-vivre-avec-un-traitement-anticoagulant";
    var lienSuiviAnticoagulantsEmbolie = "/suivi/embolie-pulmonaire#bien-vivre-avec-un-traitement-anticoagulant";

    
    // Type de navigation - Au clic de "Oui", affichage de "Pathologie"
    $('#modal-body-entree-oui').click(function(){
        $('.modal-body-entree').hide();
        $('.modal-body-pathologie').show();
    });


    // Type de navigation - Au clic, prend en compte "Artérite des membres inférieurs" pour l'ensemble du formulaire
    $('.modal-body #modal-body-pathologie-ami').click(function(){
        $(this).addClass('pathologie-active');
        $('.modal-body-pathologie').hide();
        $('.modal-body-parcours-info-min').show();
        $('#modal-body-comprendre-first-causes').attr('href', lienCausesArterite);
        $('#modal-body-comprendre-third-full-symptomes').attr('href', lienSymptomesArterite);
        $('#modal-body-comprendre-third-compliquations').attr('href', lienCompliquationsArterite);
        $('#modal-body-pec-first-mc-mdv').attr('href', lienModifModeArterite);
        $('#modal-body-pec-first-mc-med').attr('href', lienPecMedArterite);
    });
    
    // Type de navigation - Au clic, prend en compte "Maladie coronaire" pour l'ensemble du formulaire
    $('.modal-body #modal-body-pathologie-mc').click(function(){
        $(this).addClass('pathologie-active');
        $('.modal-body-pathologie').hide();
        $('.modal-body-parcours-info-min').show();
        $('#modal-body-comprendre-first-causes').attr('href', lienCausesMaladieC);
        $('#modal-body-comprendre-third-full-symptomes').attr('href', lienSymptomesMaladieC);
        $('#modal-body-comprendre-third-compliquations').attr('href', lienCompliquationsMaladieC);
        $('#modal-body-pec-first-mc-mdv').attr('href', lienModifModeMaladieC);
        $('#modal-body-pec-first-mc-med').attr('href', lienPecMedMaladieC);
    });

    // Type de navigation - Au clic, prend en compte "Arythmie cardiaque" pour l'ensemble du formulaire
    $('.modal-body #modal-body-pathologie-ac').click(function(){
        $(this).addClass('pathologie-active');
        $('.modal-body-pathologie').hide();
        $('.modal-body-parcours-info-full').show();
        $('#modal-body-comprendre-first-causes').attr('href', lienCausesArythmie);
        $('#modal-body-comprendre-sec-diag').attr('href', lienDiagnosticArythmie);
        $('#modal-body-comprendre-third-full-symptomes').attr('href', lienSymptomesArythmie);
        $('#modal-body-comprendre-third-compliquations').attr('href', lienCompliquationsArythmie);
        $('#modal-body-pec-first-full-diff').attr('href', lienDiffTraitArythmie);
        $('#modal-body-pec-first-full-antico').attr('href', lienPecAnticoagulantsArythmie);
        $('#modal-body-suivi-first-pe').attr('href', lienAssociationsArythmie);
        $('#modal-body-suivi-first-traitement').attr('href', lienSuiviAnticoagulantsArythmie);
    });

    // Type de navigation - Au clic, prend en compte "AVC" pour l'ensemble du formulaire
    $('.modal-body #modal-body-pathologie-avc').click(function(){
        $(this).addClass('pathologie-active');
        $('.modal-body-pathologie').hide();
        $('.modal-body-parcours-info-full').show();
        $('#modal-body-comprendre-first-causes').attr('href', lienCausesAVC);
        $('#modal-body-comprendre-third-full-symptomes').attr('href', lienSymptomesAVC);
        $('#modal-body-comprendre-third-compliquations').attr('href', lienCompliquationsAVC);
        $('#modal-body-pec-first-avc-demarches').attr('href', lienDemarchesAVC);
        $('#modal-body-pec-sec-avc-apres').attr('href', lienApresAVC);
        $('#modal-body-pec-sec-avc-prev').attr('href', lienPreventionAVC);
        $('#modal-body-suivi-first-pe').attr('href', lienAssociationsAVC);
        $('#modal-body-suivi-first-traitement').attr('href', lienSuiviAnticoagulantsAVC);
    });

    // Type de navigation - Au clic, prend en compte "Phlébite" pour l'ensemble du formulaire
    $('.modal-body #modal-body-pathologie-phlebite').click(function(){
        $(this).addClass('pathologie-active');
        $('.modal-body-pathologie').hide();
        $('.modal-body-parcours-info-full').show();
        $('#modal-body-comprendre-first-causes').attr('href', lienCausesPhlebite);
        $('#modal-body-comprendre-sec-diag').attr('href', lienDiagnosticPhlebite);
        $('#modal-body-comprendre-third-full-symptomes').attr('href', lienSymptomesPhlebite);
        $('#modal-body-comprendre-third-compliquations').attr('href', lienCompliquationsPhlebite);
        $('#modal-body-pec-first-full-diff').attr('href', lienDiffTraitPhlebite);
        $('#modal-body-pec-first-full-antico').attr('href', lienPecAnticoagulantsPhlebite);
        $('#modal-body-suivi-first-pe').attr('href', lienAssociationsPhlebite);
        $('#modal-body-suivi-first-traitement').attr('href', lienSuiviAnticoagulantsPhlebite);
    });

    // Type de navigation - Au clic, prend en compte "Embolie pulmonaire" pour l'ensemble du formulaire
    $('.modal-body #modal-body-pathologie-ep').click(function(){
        $(this).addClass('pathologie-active');
        $('.modal-body-pathologie').hide();
        $('.modal-body-parcours-info-full').show();
        $('#modal-body-comprendre-first-causes').attr('href', lienCausesEmbolie);
        $('#modal-body-comprendre-sec-diag').attr('href', lienDiagnosticEmbolie);
        $('#modal-body-comprendre-third-full-symptomes').attr('href', lienSymptomesEmbolie);
        $('#modal-body-comprendre-third-compliquations').attr('href', lienCompliquationsEmbolie);
        $('#modal-body-pec-first-full-diff').attr('href', lienDiffTraitEmbolie);
        $('#modal-body-pec-first-full-antico').attr('href', lienPecAnticoagulantsEmbolie);
        $('#modal-body-suivi-first-pe').attr('href', lienAssociationsEmbolie);
        $('#modal-body-suivi-first-traitement').attr('href', lienSuiviAnticoagulantsEmbolie);
    });


    //Comprendre
    // Au clic, affiche le terminateur 1 tous sauf Artérite et Maladie coronaire
    $('.modal-body #modal-body-info-full-comprendre').click(function(){
        $('.modal-body-parcours-info-full').hide();
        $('.modal-body-comprendre-first').show();
    });
    // Au clic, affiche le terminateur 1 Artérite et Maladie coronaire
    $('.modal-body #modal-body-info-min-comprendre').click(function(){
        $('.modal-body-parcours-info-min').hide();
        $('.modal-body-comprendre-first').show();
    });
    // Au clic, affiche le terminateur 2 ou 3 selon pathologie
    $('.modal-body #modal-body-comprendre-first-consequences').click(function(){
        $('.modal-body-comprendre-first').hide();
        if ($('.modal-body #modal-body-pathologie-ami').hasClass("pathologie-active") || $('.modal-body #modal-body-pathologie-mc').hasClass("pathologie-active") || $('.modal-body #modal-body-pathologie-avc').hasClass("pathologie-active")) {
            $('.modal-body-comprendre-third').show();
        } else {
            $('.modal-body-comprendre-sec').show();
        }
    });
    // Au clic, affiche le terminateur 3 selon pathologie
    $('.modal-body #modal-body-comprendre-sec-signes').click(function(){
        $('.modal-body-comprendre-sec').hide();
        $('.modal-body-comprendre-third').show();
    });


    // Prise en charge
    // Au clic, affiche le terminateur 1 tous sauf Artérite et Maladie coronaire
    $('.modal-body #modal-body-info-full-pec').click(function(){
        $('.modal-body-parcours-info-full').hide();
        if ($('.modal-body #modal-body-pathologie-avc').hasClass("pathologie-active")) {
            $('.modal-body-pec-first-avc').show();
        } else {
            $('.modal-body-pec-first-full').show();
        }
    });
    // Au clic, affiche le terminateur 1 Artérite et Maladie coronaire
    $('.modal-body #modal-body-info-min-pec').click(function(){
        $('.modal-body-parcours-info-min').hide();
        $('.modal-body-pec-first-mc').show();
    });
    // AVC - Au clic, affiche le terminateur 2
    $('.modal-body #modal-body-pec-first-avc-medic').click(function(){
        $('.modal-body-pec-first-avc').hide();
        $('.modal-body-pec-sec-avc').show();
    });


    // Suivi
    // Au clic, affiche le terminateur 1
    $('.modal-body #modal-body-info-full-suivi').click(function(){
        $('.modal-body-parcours-info-full').hide();
        $('.modal-body-suivi-first').show();
    });

})(jQuery, Drupal);