(function ($, Drupal) {

    'use strict';

    /* Gestion formulaire AVC */
    // Base réponses
    var qStartVrai = '<p class="titre-rep-ok">Exact !</p><p class="texte-rep-ok">';
    var qStartFaux = '<p class="titre-rep-nok">Mauvaise Réponse !</p><p class="texte-rep-ok">';
    // Réponse Q1
    var q1Rep = 'Réponses A et D :</p><p>L\'AVC ou Accident Vasculaire Cérébral est secondaire à une lésion d\'un vaisseau du cerveau.</p><p>Il existe 2 formes d\'AVC :</p><p>- Le plus souvent, l\'AVC est provoqué par un caillot qui obstrue une artère et empêche le passage du sang. On parle d\'infarctus cérébral ou d\’AVC ischémique.<br />- Moins fréquemment, c\’est une artère du cerveau qui se rompt. On parle d\’AVC hémorragique.<br /></p><p>L\’AVC est une «souffrance brutale» d\’une région du cerveau ! Une artère du cerveau soit «se bouche» soit «se rompt».</p>';
    var q1Vrai = qStartVrai+q1Rep;
    var q1Faux = qStartFaux+q1Rep;
    // Réponse Q2
    var q2Rep = 'Réponses A, C et D :</p><p>L\’AIT ou Accident Ischémique Transitoire dure de quelques minutes à quelques heures.</p><p>Les symptômes (paralysie, trouble du langage ou trouble visuel) surviennent brutalement et sont transitoires. L\’AIT représente un véritable signe d\’alerte et nécessite une consultation médicale en urgence.</p><p>Il faut appeler immédiatement le 15.</p>';
    var q2Vrai = qStartVrai+q2Rep;
    var q2Faux = qStartFaux+q2Rep;
    // Réponse Q3
    var q3Rep = 'Réponses A, C et D :</p><p>Les symptômes d\’un AVC apparaissent brutalement.</p><p>Les plus fréquents sont :</p><p>- Le visage paralysé<br />- Une inertie (paralysie) d\’un membre<br />- Un trouble de la parole</p><p>Il s\'agit d\'une urgence, il faut appeler le 15</p><p>D\'autres symptômes peuvent être associés, comme un trouble de l\'équilibre, de la vision ou de la compréhension. Les maux de tête ne sont pas constants.</p>';
    var q3Vrai = qStartVrai+q3Rep;
    var q3Faux = qStartFaux+q3Rep;
    // Réponse Q4
    var q4Rep = 'Réponse B :</p><p>Toute suspicion d\'AVC (incluant les AIT) nécessite l\'appel téléphonique immédiat au Centre 15 (SAMU et pompiers) pour assurer le transport du patient vers un centre hospitalier adapté. Le pronostic dépend de la rapidité de la prise en charge. Plus le traitement commence tôt, plus il sera efficace.</p><p>L\'AVC est une urgence qui nécessite une hospitalisation et un traitement le plus rapide possible, dans les toutes premières heures suivant la survenue des premiers symptômes. VITE ! IL FAUT APPELER LE 15 SAMU OU LE 112 DEPUIS SON PORTABLE en France ou en Europe.</p>';
    var q4Vrai = qStartVrai+q4Rep;
    var q4Faux = qStartFaux+q4Rep;
    // Réponse Q5
    var q5Rep = 'Réponses B et C :</p><p>Les facteurs de risque favorisant la survenue d\'un AVC sont :</p><p>- L\'hypertension artérielle,<br />- La fibrillation atriale (arythmie cardiaque),<br />- Le diabète,<br />- L\'augmentation du taux de cholestérol sanguin,<br />- Le tabagisme, l\'usage de drogues et la consommation excessive d\'alcool et la prise de certains médicaments<br />- La sédentarité et l\'inactivité physique,<br />- L\'obésité et une alimentation mal équilibrée.<br />- Les anomalies (excès ou déficit) de la coagulation du sang</p><p>L\'AVC ne frappe pas que les personnes âgées. Chaque année près de 12 000 personnes de moins de 45 ans sont touchées.</p>';
    var q5Vrai = qStartVrai+q5Rep;
    var q5Faux = qStartFaux+q5Rep;
    // Réponse Q6
    var q6Rep = 'Réponses A, B, C et D :</p><p>L\'AVC représente :</p><p>- La 1ère cause de handicap physique acquis chez l\'adulte<br />- La 1ère cause de décès chez la femme<br />- La 2ème cause de déclin intellectuel après la maladie d\'Alzheimer<br />- La 3ème cause de décès sur l\'ensemble de la population après l\'infactus du myocarde et le cancer (souvent la première dans bon nombre de département)</p>';
    var q6Vrai = qStartVrai+q6Rep;
    var q6Faux = qStartFaux+q6Rep;
    // Réponse Q7
    var q7Rep = 'Réponses A, B et C :</p><p>L\'AVC est une urgence qui nécessite une hospitalisation et un traitement le plus rapide possible (dans les toutes premières heures suivant la survenue des premiers symptômes). La prise en charge en urgence peut être faite par thrombolyse ou thrombectomie.</p><p>- La thrombolyse consiste à désagréger le caillot sanguin par un médicament. Le patient reçoit une injection qui va dissoudre le caillot bouchant l\'artère cérébrale.<br />- La thrombectomie consiste à retirer le caillot sanguin à l\'aide d\'une sonde.</p><p>Quel que soit le traitement, thrombolyse et/ou thrombectomie, le délai d\'intervention maximum se situe donc entre 4h30 à 6h suivant le traitement appliqué à l\'AVC.</p>';
    var q7Vrai = qStartVrai+q7Rep;
    var q7Faux = qStartFaux+q7Rep;
    // Réponse Q8
    var q8Rep = 'Réponses A et C :</p><p>Environ 20 % des patients ayant eu un AVC seront victimes d\'une récidive dans les 5 ans.</p><p>Il est possible de réduire ce risque de nouvel AVC en adaptant son mode de vie, en contrôlant ses facteurs de risque et en respectant le traitement prescrit par son médecin.</p>';
    var q8Vrai = qStartVrai+q8Rep;
    var q8Faux = qStartFaux+q8Rep;
    // Réponse Q9
    var q9Rep = 'Réponses A, B et C :</p><p>Les traitements préventifs visent à réduire le risque de faire un nouvel AVC. Ils consistent à prendre en charge les facteurs de risques.</p><p>Après un infarctus ischémique, la prise d\'un anti-thrombotique (antiagrégant plaquettaire ou d\'un anticoagulant selon la cause de l\'AVC) permet aussi de réduire ce risque.</p>';
    var q9Vrai = qStartVrai+q9Rep;
    var q9Faux = qStartFaux+q9Rep;
    // Réponse Q10
    var q10Rep = 'Réponse D :</p><p>Pour éviter une récidive d\'AVC il convient :</p><p>- D\'arrêter de fumer : il n\'existe pas de seuil en dessous duquel fumer ne représente pas de risque. L\'arrêt complet du tabac est la seule recommandation.<br />- De pratiquer une activité physique régulière : l\'activité physique joue un rôle important pour réduire le risque de récidive d\'AVC. Il est important qu\'elle soit quotidienne, d\'intensité modérée, d\'une durée d\'au moins 30 min et adaptée aux possibilités de chacun.<br />- De surveiller son alimentation : il faut manger équilibré et limiter les matières grasses, le sucre, le sel et l\'alcool.</p>';
    var q10Vrai = qStartVrai+q10Rep;
    var q10Faux = qStartFaux+q10Rep;
    // Réponse Q11
    var q11Rep = 'Réponses A, C et D :</p><p>En fonction de la zone du cerveau touchée, une rééducation motrice et/ou orthophonique peut être nécessaire. Elle vise à guider la récupération et à favoriser le retour à une vie normale.</p>';
    var q11Vrai = qStartVrai+q11Rep;
    var q11Faux = qStartFaux+q11Rep;
    // Réponse Q12
    var q12Rep = 'Réponses B et C :</p><p>Un anticoagulant est un médicament qui réduit la formation de caillots sanguins et le risque d\'une récidive d\'AVC.</p>';
    var q12Vrai = qStartVrai+q12Rep;
    var q12Faux = qStartFaux+q12Rep;
    // Réponse Q13
    var q13Rep = 'Aucune réponse :</p><p>Il ne faut jamais arrêter le traitement sans avis médical.</p>';
    var q13Vrai = qStartVrai+q13Rep;
    var q13Faux = qStartFaux+q13Rep;
    // Réponse Q14
    var q14Rep = 'Réponses A, B et C :</p><p>Il existe plusieurs moyens mnémotechniques :</p><p>- Toujours prendre le traitement à heure fixe<br />- Associer la prise du traitement à un geste quotidien/reflexe<br />- Utiliser un pilulier<br />- Mettre une alarme<br />- Ne pas oublier de renouveler l\'ordonnance avant la fin du traitement pour ne pas l\'interrompre.<br />- Etc…</p>';
    var q14Vrai = qStartVrai+q14Rep;
    var q14Faux = qStartFaux+q14Rep;
    // Réponse Q15
    var q15Rep = 'Réponse D :</p><p>La conduite à tenir est variable en fonction du médicament administré.</p><p>En cas de vomissement, il faut contacter le médecin ou le pharmacien pour connaître la conduite à tenir.</p><p>En aucun cas il ne faut prendre une double dose !</p>';
    var q15Vrai = qStartVrai+q15Rep;
    var q15Faux = qStartFaux+q15Rep;
    // Réponse Q16
    var q16Rep = 'Réponse D :</p><p>En cas d\'oubli, il faut toujours se reporter à la notice du traitement et demander conseil au médecin ou au pharmacien.</p><p>La non-observance ou mauvaise observance favorise le risque de récidive d\'un AVC.</p><p>Il ne faut pas compenser l\'oubli de la prise du médicament par une prise de « rattrapage » au-delà de la dose habituelle recommandée.</p>';
    var q16Vrai = qStartVrai+q16Rep;
    var q16Faux = qStartFaux+q16Rep;
    // Réponse Q17
    var q17Rep = 'Réponse A :</p><p>Le traitement doit être pris chaque jour.</p><p>En cas d\'oubli de prises, le risque de récidive est majeur.</p>';
    var q17Vrai = qStartVrai+q17Rep;
    var q17Faux = qStartFaux+q17Rep;
    // Réponse Q18
    var q18Rep = 'Réponse C et D :</p><p>Il est important de porter  sur soi avec les papiers d\'identité une carte « Patient sous Anticoagulant ».</p><p>Elle sert à informer les équipes soignantes :</p><p>- En cas d\'inconscience lors d\'un accident nécessitant des soins (exemple accident de la voie publique, etc)<br />- Pour adapter les soins en fonction du type de molécule<br />- Dans le cadre de consultations réglées avec un nouvel intervenant, cela évite le risque de prescription de médicaments susceptibles d\'induire des interactions</p>';
    var q18Vrai = qStartVrai+q18Rep;
    var q18Faux = qStartFaux+q18Rep;
    // Réponse Q19
    var q19Rep = 'Réponses A et D :</p><p>Il faut être prudent pour éviter les chutes :  (car le fait d\'être sous anticoagulant aura tendance à vous faire plus saigner (plaie ouverte et/ou hématome)</p><p>- Eviter l\'encombrement au sol,<br />- Essuyer tout ce qui pourrait rendre le sol glissant,<br />- Mettre un tapis antidérapant dans la douche.</p><p>Il faut bien choisir ses activités sportives :</p><p>- Eviter les sports de combat, le rugby,<br />- Limiter le VTT et le ski,<br />- Privilégier la natation, la marche.</p>';
    var q19Vrai = qStartVrai+q19Rep;
    var q19Faux = qStartFaux+q19Rep;
    // Réponse Q20
    var q20Rep = 'Réponses B et C :</p><p>Il faut être vigilant sur tout saignement extériorisé comme :</p><p>- sang dans les selles<br />- sang dans les urines<br />- saignement gynécologique<br />- en se brossant les dents<br />- des crachats ou des vomissements sanglants</p><p>Il faut être attentif aux signes d\'alerte de saignements non visibles que sont la fatigue inhabituelle, l\'essoufflement, la pâleur, le coeur qui bat trop vite.</p><p>En cas de signes ou symptômes de saignements (inhabituels, abondants ou prolongés) ou en cas de modification de l\'état général, il faut informer immédiatement le médecin ou le pharmacien.</p>';
    var q20Vrai = qStartVrai+q20Rep;
    var q20Faux = qStartFaux+q20Rep;




    // Q1, au clic du bouton de validation, vérifie la réponse et affiche le résultat
    $('#quizz-avc .quizz-custom-wrapper[data-question="question1"] #confirmer-reponse').click(function(){
        var reponse = $('#quizz-avc .quizz-question .active button').attr('data-reponse');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question1"] .reponse-a button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question1"] .reponse-b button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question1"] .reponse-c button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question1"] .reponse-d button').prop('disabled', true);
        if (reponse == "A") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question1"] .reponse-a').addClass('reponse-validee-ok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question1"] .quizz-question-reponse').append(q1Vrai);
        } else if (reponse == "B") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question1"] .reponse-b').addClass('reponse-validee-nok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question1"] .quizz-question-reponse').append(q1Faux);
        } else if (reponse == "C") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question1"] .reponse-c').addClass('reponse-validee-nok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question1"] .quizz-question-reponse').append(q1Faux);
        } else {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question1"] .reponse-d').addClass('reponse-validee-ok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question1"] .quizz-question-reponse').append(q1Vrai);
        }
        $('#quizz-avc .quizz-custom-wrapper[data-question="question1"] #confirmer-reponse').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question1"] #question-suivante').show();
    });
    // Q1, passage à la question suivante
    $('#quizz-avc .quizz-custom-wrapper[data-question="question1"] #question-suivante').click(function(){
        $('#quizz-avc .quizz-custom-wrapper[data-question="question1"]').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question1"]').removeClass('active');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question2"]').show();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question2"]').addClass('active');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question2"] #question-suivante').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question2"] #confirmer-reponse').prop('disabled', true);
        $('#quizz-avc .quizz-dot-2').addClass('active');
    });


    // Q2, au clic du bouton de validation, vérifie la réponse et affiche le résultat
    $('#quizz-avc .quizz-custom-wrapper[data-question="question2"] #confirmer-reponse').click(function(){
        var reponse = $('#quizz-avc .quizz-question .active button').attr('data-reponse');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question2"] .reponse-a button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question2"] .reponse-b button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question2"] .reponse-c button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question2"] .reponse-d button').prop('disabled', true);
        if (reponse == "A") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question2"] .reponse-a').addClass('reponse-validee-ok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question2"] .quizz-question-reponse').append(q2Vrai);
        } else if (reponse == "B") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question2"] .reponse-b').addClass('reponse-validee-nok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question2"] .quizz-question-reponse').append(q2Faux);
        } else if (reponse == "C") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question2"] .reponse-c').addClass('reponse-validee-ok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question2"] .quizz-question-reponse').append(q2Vrai);
        } else {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question2"] .reponse-d').addClass('reponse-validee-ok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question2"] .quizz-question-reponse').append(q2Vrai);
        }
        $('#quizz-avc .quizz-custom-wrapper[data-question="question2"] #confirmer-reponse').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question2"] #question-suivante').show();
    });
    // Q2, passage à la question suivante
    $('#quizz-avc .quizz-custom-wrapper[data-question="question2"] #question-suivante').click(function(){
        $('#quizz-avc .quizz-custom-wrapper[data-question="question2"]').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question2"]').removeClass('active');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question3"]').show();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question3"]').addClass('active');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question3"] #question-suivante').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question3"] #confirmer-reponse').prop('disabled', true);
        $('#quizz-avc .quizz-dot-3').addClass('active');
    });


    // Q3, au clic du bouton de validation, vérifie la réponse et affiche le résultat
    $('#quizz-avc .quizz-custom-wrapper[data-question="question3"] #confirmer-reponse').click(function(){
        var reponse = $('#quizz-avc .quizz-question .active button').attr('data-reponse');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question3"] .reponse-a button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question3"] .reponse-b button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question3"] .reponse-c button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question3"] .reponse-d button').prop('disabled', true);
        if (reponse == "A") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question3"] .reponse-a').addClass('reponse-validee-ok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question3"] .quizz-question-reponse').append(q3Vrai);
        } else if (reponse == "B") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question3"] .reponse-b').addClass('reponse-validee-nok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question3"] .quizz-question-reponse').append(q3Faux);
        } else if (reponse == "C") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question3"] .reponse-c').addClass('reponse-validee-ok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question3"] .quizz-question-reponse').append(q3Vrai);
        } else {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question3"] .reponse-d').addClass('reponse-validee-ok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question3"] .quizz-question-reponse').append(q3Vrai);
        }
        $('#quizz-avc .quizz-custom-wrapper[data-question="question3"] #confirmer-reponse').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question3"] #question-suivante').show();
    });
    // Q3, passage à la question suivante
    $('#quizz-avc .quizz-custom-wrapper[data-question="question3"] #question-suivante').click(function(){
        $('#quizz-avc .quizz-custom-wrapper[data-question="question3"]').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question3"]').removeClass('active');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question4"]').show();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question4"]').addClass('active');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question4"] #question-suivante').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question4"] #confirmer-reponse').prop('disabled', true);
        $('#quizz-avc .quizz-dot-4').addClass('active');
    });


    // Q4, au clic du bouton de validation, vérifie la réponse et affiche le résultat
    $('#quizz-avc .quizz-custom-wrapper[data-question="question4"] #confirmer-reponse').click(function(){
        var reponse = $('#quizz-avc .quizz-question .active button').attr('data-reponse');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question4"] .reponse-a button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question4"] .reponse-b button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question4"] .reponse-c button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question4"] .reponse-d button').prop('disabled', true);
        if (reponse == "A") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question4"] .reponse-a').addClass('reponse-validee-nok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question4"] .quizz-question-reponse').append(q4Faux);
        } else if (reponse == "B") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question4"] .reponse-b').addClass('reponse-validee-ok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question4"] .quizz-question-reponse').append(q4Vrai);
        } else if (reponse == "C") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question4"] .reponse-c').addClass('reponse-validee-nok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question4"] .quizz-question-reponse').append(q4Faux);
        } else {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question4"] .reponse-d').addClass('reponse-validee-nok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question4"] .quizz-question-reponse').append(q4Faux);
        }
        $('#quizz-avc .quizz-custom-wrapper[data-question="question4"] #confirmer-reponse').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question4"] #question-suivante').show();
    });
    // Q4, passage à la question suivante
    $('#quizz-avc .quizz-custom-wrapper[data-question="question4"] #question-suivante').click(function(){
        $('#quizz-avc .quizz-custom-wrapper[data-question="question4"]').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question4"]').removeClass('active');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question5"]').show();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question5"]').addClass('active');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question5"] #question-suivante').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question5"] #confirmer-reponse').prop('disabled', true);
        $('#quizz-avc .quizz-dot-5').addClass('active');
    });


    // Q5, au clic du bouton de validation, vérifie la réponse et affiche le résultat
    $('#quizz-avc .quizz-custom-wrapper[data-question="question5"] #confirmer-reponse').click(function(){
        var reponse = $('#quizz-avc .quizz-question .active button').attr('data-reponse');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question5"] .reponse-a button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question5"] .reponse-b button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question5"] .reponse-c button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question5"] .reponse-d button').prop('disabled', true);
        if (reponse == "A") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question5"] .reponse-a').addClass('reponse-validee-nok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question5"] .quizz-question-reponse').append(q5Faux);
        } else if (reponse == "B") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question5"] .reponse-b').addClass('reponse-validee-ok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question5"] .quizz-question-reponse').append(q5Vrai);
        } else if (reponse == "C") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question5"] .reponse-c').addClass('reponse-validee-ok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question5"] .quizz-question-reponse').append(q5Vrai);
        } else {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question5"] .reponse-d').addClass('reponse-validee-nok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question5"] .quizz-question-reponse').append(q5Faux);
        }
        $('#quizz-avc .quizz-custom-wrapper[data-question="question5"] #confirmer-reponse').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question5"] #question-suivante').show();
    });
    // Q5, passage à la question suivante
    $('#quizz-avc .quizz-custom-wrapper[data-question="question5"] #question-suivante').click(function(){
        $('#quizz-avc .quizz-custom-wrapper[data-question="question5"]').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question5"]').removeClass('active');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question6"]').show();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question6"]').addClass('active');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question6"] #question-suivante').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question6"] #confirmer-reponse').prop('disabled', true);
        $('#quizz-avc .quizz-dot-6').addClass('active');
    });


    // Q6, au clic du bouton de validation, vérifie la réponse et affiche le résultat
    $('#quizz-avc .quizz-custom-wrapper[data-question="question6"] #confirmer-reponse').click(function(){
        var reponse = $('#quizz-avc .quizz-question .active button').attr('data-reponse');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question6"] .reponse-a button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question6"] .reponse-b button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question6"] .reponse-c button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question6"] .reponse-d button').prop('disabled', true);
        if (reponse == "A") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question6"] .reponse-a').addClass('reponse-validee-ok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question6"] .quizz-question-reponse').append(q6Vrai);
        } else if (reponse == "B") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question6"] .reponse-b').addClass('reponse-validee-ok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question6"] .quizz-question-reponse').append(q6Vrai);
        } else if (reponse == "C") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question6"] .reponse-c').addClass('reponse-validee-ok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question6"] .quizz-question-reponse').append(q6Vrai);
        } else {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question6"] .reponse-d').addClass('reponse-validee-ok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question6"] .quizz-question-reponse').append(q6Vrai);
        }
        $('#quizz-avc .quizz-custom-wrapper[data-question="question6"] #confirmer-reponse').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question6"] #question-suivante').show();
    });
    // Q6, passage à la question suivante
    $('#quizz-avc .quizz-custom-wrapper[data-question="question6"] #question-suivante').click(function(){
        $('#quizz-avc .quizz-custom-wrapper[data-question="question6"]').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question6"]').removeClass('active');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question7"]').show();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question7"]').addClass('active');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question7"] #question-suivante').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question7"] #confirmer-reponse').prop('disabled', true);
        $('#quizz-avc .quizz-dot-7').addClass('active');
    });


    // Q7, au clic du bouton de validation, vérifie la réponse et affiche le résultat
    $('#quizz-avc .quizz-custom-wrapper[data-question="question7"] #confirmer-reponse').click(function(){
        var reponse = $('#quizz-avc .quizz-question .active button').attr('data-reponse');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question7"] .reponse-a button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question7"] .reponse-b button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question7"] .reponse-c button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question7"] .reponse-d button').prop('disabled', true);
        if (reponse == "A") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question7"] .reponse-a').addClass('reponse-validee-ok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question7"] .quizz-question-reponse').append(q7Vrai);
        } else if (reponse == "B") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question7"] .reponse-b').addClass('reponse-validee-ok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question7"] .quizz-question-reponse').append(q7Vrai);
        } else if (reponse == "C") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question7"] .reponse-c').addClass('reponse-validee-ok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question7"] .quizz-question-reponse').append(q7Vrai);
        } else {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question7"] .reponse-d').addClass('reponse-validee-nok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question7"] .quizz-question-reponse').append(q7Faux);
        }
        $('#quizz-avc .quizz-custom-wrapper[data-question="question7"] #confirmer-reponse').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question7"] #question-suivante').show();
    });
    // Q7, passage à la question suivante
    $('#quizz-avc .quizz-custom-wrapper[data-question="question7"] #question-suivante').click(function(){
        $('#quizz-avc .quizz-custom-wrapper[data-question="question7"]').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question7"]').removeClass('active');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question8"]').show();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question8"]').addClass('active');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question8"] #question-suivante').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question8"] #confirmer-reponse').prop('disabled', true);
        $('#quizz-avc .quizz-dot-8').addClass('active');
    });


    // Q8, au clic du bouton de validation, vérifie la réponse et affiche le résultat
    $('#quizz-avc .quizz-custom-wrapper[data-question="question8"] #confirmer-reponse').click(function(){
        var reponse = $('#quizz-avc .quizz-question .active button').attr('data-reponse');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question8"] .reponse-a button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question8"] .reponse-b button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question8"] .reponse-c button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question8"] .reponse-d button').prop('disabled', true);
        if (reponse == "A") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question8"] .reponse-a').addClass('reponse-validee-ok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question8"] .quizz-question-reponse').append(q8Vrai);
        } else if (reponse == "B") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question8"] .reponse-b').addClass('reponse-validee-nok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question8"] .quizz-question-reponse').append(q8Faux);
        } else if (reponse == "C") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question8"] .reponse-c').addClass('reponse-validee-ok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question8"] .quizz-question-reponse').append(q8Vrai);
        } else {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question8"] .reponse-d').addClass('reponse-validee-nok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question8"] .quizz-question-reponse').append(q8Faux);
        }
        $('#quizz-avc .quizz-custom-wrapper[data-question="question8"] #confirmer-reponse').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question8"] #question-suivante').show();
    });
    // Q8, passage à la question suivante
    $('#quizz-avc .quizz-custom-wrapper[data-question="question8"] #question-suivante').click(function(){
        $('#quizz-avc .quizz-custom-wrapper[data-question="question8"]').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question8"]').removeClass('active');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question9"]').show();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question9"]').addClass('active');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question9"] #question-suivante').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question9"] #confirmer-reponse').prop('disabled', true);
        $('#quizz-avc .quizz-dot-9').addClass('active');
    });


    // Q9, au clic du bouton de validation, vérifie la réponse et affiche le résultat
    $('#quizz-avc .quizz-custom-wrapper[data-question="question9"] #confirmer-reponse').click(function(){
        var reponse = $('#quizz-avc .quizz-question .active button').attr('data-reponse');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question9"] .reponse-a button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question9"] .reponse-b button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question9"] .reponse-c button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question9"] .reponse-d button').prop('disabled', true);
        if (reponse == "A") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question9"] .reponse-a').addClass('reponse-validee-ok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question9"] .quizz-question-reponse').append(q9Vrai);
        } else if (reponse == "B") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question9"] .reponse-b').addClass('reponse-validee-ok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question9"] .quizz-question-reponse').append(q9Vrai);
        } else if (reponse == "C") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question9"] .reponse-c').addClass('reponse-validee-ok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question9"] .quizz-question-reponse').append(q9Vrai);
        } else {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question9"] .reponse-d').addClass('reponse-validee-nok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question9"] .quizz-question-reponse').append(q9Faux);
        }
        $('#quizz-avc .quizz-custom-wrapper[data-question="question9"] #confirmer-reponse').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question9"] #question-suivante').show();
    });
    // Q9, passage à la question suivante
    $('#quizz-avc .quizz-custom-wrapper[data-question="question9"] #question-suivante').click(function(){
        $('#quizz-avc .quizz-custom-wrapper[data-question="question9"]').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question9"]').removeClass('active');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question10"]').show();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question10"]').addClass('active');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question10"] #question-suivante').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question10"] #confirmer-reponse').prop('disabled', true);
        $('#quizz-avc .quizz-dot-10').addClass('active');
    });


    // Q10, au clic du bouton de validation, vérifie la réponse et affiche le résultat
    $('#quizz-avc .quizz-custom-wrapper[data-question="question10"] #confirmer-reponse').click(function(){
        var reponse = $('#quizz-avc .quizz-question .active button').attr('data-reponse');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question10"] .reponse-a button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question10"] .reponse-b button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question10"] .reponse-c button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question10"] .reponse-d button').prop('disabled', true);
        if (reponse == "A") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question10"] .reponse-a').addClass('reponse-validee-nok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question10"] .quizz-question-reponse').append(q10Faux);
        } else if (reponse == "B") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question10"] .reponse-b').addClass('reponse-validee-nok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question10"] .quizz-question-reponse').append(q10Faux);
        } else if (reponse == "C") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question10"] .reponse-c').addClass('reponse-validee-nok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question10"] .quizz-question-reponse').append(q10Faux);
        } else {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question10"] .reponse-d').addClass('reponse-validee-ok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question10"] .quizz-question-reponse').append(q10Vrai);
        }
        $('#quizz-avc .quizz-custom-wrapper[data-question="question10"] #confirmer-reponse').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question10"] #question-suivante').show();
    });
    // Q10, passage à la question suivante
    $('#quizz-avc .quizz-custom-wrapper[data-question="question10"] #question-suivante').click(function(){
        $('#quizz-avc .quizz-custom-wrapper[data-question="question10"]').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question10"]').removeClass('active');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question11"]').show();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question11"]').addClass('active');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question11"] #question-suivante').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question11"] #confirmer-reponse').prop('disabled', true);
        $('#quizz-avc .quizz-dot-11').addClass('active');
    });


    // Q11, au clic du bouton de validation, vérifie la réponse et affiche le résultat
    $('#quizz-avc .quizz-custom-wrapper[data-question="question11"] #confirmer-reponse').click(function(){
        var reponse = $('#quizz-avc .quizz-question .active button').attr('data-reponse');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question11"] .reponse-a button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question11"] .reponse-b button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question11"] .reponse-c button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question11"] .reponse-d button').prop('disabled', true);
        if (reponse == "A") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question11"] .reponse-a').addClass('reponse-validee-ok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question11"] .quizz-question-reponse').append(q11Vrai);
        } else if (reponse == "B") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question11"] .reponse-b').addClass('reponse-validee-nok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question11"] .quizz-question-reponse').append(q11Faux);
        } else if (reponse == "C") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question11"] .reponse-c').addClass('reponse-validee-ok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question11"] .quizz-question-reponse').append(q11Vrai);
        } else {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question11"] .reponse-d').addClass('reponse-validee-ok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question11"] .quizz-question-reponse').append(q11Vrai);
        }
        $('#quizz-avc .quizz-custom-wrapper[data-question="question11"] #confirmer-reponse').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question11"] #question-suivante').show();
    });
    // Q11, passage à la question suivante
    $('#quizz-avc .quizz-custom-wrapper[data-question="question11"] #question-suivante').click(function(){
        $('#quizz-avc .quizz-custom-wrapper[data-question="question11"]').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question11"]').removeClass('active');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question12"]').show();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question12"]').addClass('active');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question12"] #question-suivante').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question12"] #confirmer-reponse').prop('disabled', true);
        $('#quizz-avc .quizz-dot-12').addClass('active');
    });


    // Q12, au clic du bouton de validation, vérifie la réponse et affiche le résultat
    $('#quizz-avc .quizz-custom-wrapper[data-question="question12"] #confirmer-reponse').click(function(){
        var reponse = $('#quizz-avc .quizz-question .active button').attr('data-reponse');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question12"] .reponse-a button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question12"] .reponse-b button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question12"] .reponse-c button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question12"] .reponse-d button').prop('disabled', true);
        if (reponse == "A") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question12"] .reponse-a').addClass('reponse-validee-nok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question12"] .quizz-question-reponse').append(q12Faux);
        } else if (reponse == "B") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question12"] .reponse-b').addClass('reponse-validee-ok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question12"] .quizz-question-reponse').append(q12Vrai);
        } else if (reponse == "C") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question12"] .reponse-c').addClass('reponse-validee-ok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question12"] .quizz-question-reponse').append(q12Vrai);
        } else {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question12"] .reponse-d').addClass('reponse-validee-nok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question12"] .quizz-question-reponse').append(q12Faux);
        }
        $('#quizz-avc .quizz-custom-wrapper[data-question="question12"] #confirmer-reponse').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question12"] #question-suivante').show();
    });
    // Q12, passage à la question suivante
    $('#quizz-avc .quizz-custom-wrapper[data-question="question12"] #question-suivante').click(function(){
        $('#quizz-avc .quizz-custom-wrapper[data-question="question12"]').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question12"]').removeClass('active');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question13"]').show();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question13"]').addClass('active');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question13"] #question-suivante').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question13"] #confirmer-reponse').prop('disabled', true);
        $('#quizz-avc .quizz-dot-13').addClass('active');
    });


    // Q13, au clic du bouton de validation, vérifie la réponse et affiche le résultat
    $('#quizz-avc .quizz-custom-wrapper[data-question="question13"] #confirmer-reponse').click(function(){
        var reponse = $('#quizz-avc .quizz-question .active button').attr('data-reponse');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question13"] .reponse-a button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question13"] .reponse-b button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question13"] .reponse-c button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question13"] .reponse-d button').prop('disabled', true);
        if (reponse == "A") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question13"] .reponse-a').addClass('reponse-validee-nok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question13"] .quizz-question-reponse').append(q13Faux);
        } else if (reponse == "B") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question13"] .reponse-b').addClass('reponse-validee-nok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question13"] .quizz-question-reponse').append(q13Faux);
        } else if (reponse == "C") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question13"] .reponse-c').addClass('reponse-validee-nok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question13"] .quizz-question-reponse').append(q13Faux);
        } else {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question13"] .reponse-d').addClass('reponse-validee-nok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question13"] .quizz-question-reponse').append(q13Faux);
        }
        $('#quizz-avc .quizz-custom-wrapper[data-question="question13"] #confirmer-reponse').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question13"] #question-suivante').show();
    });
    // Q13, passage à la question suivante
    $('#quizz-avc .quizz-custom-wrapper[data-question="question13"] #question-suivante').click(function(){
        $('#quizz-avc .quizz-custom-wrapper[data-question="question13"]').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question13"]').removeClass('active');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question14"]').show();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question14"]').addClass('active');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question14"] #question-suivante').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question14"] #confirmer-reponse').prop('disabled', true);
        $('#quizz-avc .quizz-dot-14').addClass('active');
    });


    // Q14, au clic du bouton de validation, vérifie la réponse et affiche le résultat
    $('#quizz-avc .quizz-custom-wrapper[data-question="question14"] #confirmer-reponse').click(function(){
        var reponse = $('#quizz-avc .quizz-question .active button').attr('data-reponse');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question14"] .reponse-a button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question14"] .reponse-b button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question14"] .reponse-c button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question14"] .reponse-d button').prop('disabled', true);
        if (reponse == "A") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question14"] .reponse-a').addClass('reponse-validee-ok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question14"] .quizz-question-reponse').append(q14Vrai);
        } else if (reponse == "B") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question14"] .reponse-b').addClass('reponse-validee-ok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question14"] .quizz-question-reponse').append(q14Vrai);
        } else if (reponse == "C") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question14"] .reponse-c').addClass('reponse-validee-ok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question14"] .quizz-question-reponse').append(q14Vrai);
        } else {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question14"] .reponse-d').addClass('reponse-validee-nok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question14"] .quizz-question-reponse').append(q14Faux);
        }
        $('#quizz-avc .quizz-custom-wrapper[data-question="question14"] #confirmer-reponse').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question14"] #question-suivante').show();
    });
    // Q14, passage à la question suivante
    $('#quizz-avc .quizz-custom-wrapper[data-question="question14"] #question-suivante').click(function(){
        $('#quizz-avc .quizz-custom-wrapper[data-question="question14"]').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question14"]').removeClass('active');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question15"]').show();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question15"]').addClass('active');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question15"] #question-suivante').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question15"] #confirmer-reponse').prop('disabled', true);
        $('#quizz-avc .quizz-dot-15').addClass('active');
    });


    // Q15, au clic du bouton de validation, vérifie la réponse et affiche le résultat
    $('#quizz-avc .quizz-custom-wrapper[data-question="question15"] #confirmer-reponse').click(function(){
        var reponse = $('#quizz-avc .quizz-question .active button').attr('data-reponse');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question15"] .reponse-a button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question15"] .reponse-b button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question15"] .reponse-c button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question15"] .reponse-d button').prop('disabled', true);
        if (reponse == "A") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question15"] .reponse-a').addClass('reponse-validee-nok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question15"] .quizz-question-reponse').append(q15Faux);
        } else if (reponse == "B") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question15"] .reponse-b').addClass('reponse-validee-nok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question15"] .quizz-question-reponse').append(q15Faux);
        } else if (reponse == "C") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question15"] .reponse-c').addClass('reponse-validee-nok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question15"] .quizz-question-reponse').append(q15Faux);
        } else {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question15"] .reponse-d').addClass('reponse-validee-ok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question15"] .quizz-question-reponse').append(q15Vrai);
        }
        $('#quizz-avc .quizz-custom-wrapper[data-question="question15"] #confirmer-reponse').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question15"] #question-suivante').show();
    });
    // Q15, passage à la question suivante
    $('#quizz-avc .quizz-custom-wrapper[data-question="question15"] #question-suivante').click(function(){
        $('#quizz-avc .quizz-custom-wrapper[data-question="question15"]').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question15"]').removeClass('active');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question16"]').show();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question16"]').addClass('active');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question16"] #question-suivante').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question16"] #confirmer-reponse').prop('disabled', true);
        $('#quizz-avc .quizz-dot-16').addClass('active');
    });


    // Q16, au clic du bouton de validation, vérifie la réponse et affiche le résultat
    $('#quizz-avc .quizz-custom-wrapper[data-question="question16"] #confirmer-reponse').click(function(){
        var reponse = $('#quizz-avc .quizz-question .active button').attr('data-reponse');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question16"] .reponse-a button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question16"] .reponse-b button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question16"] .reponse-c button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question16"] .reponse-d button').prop('disabled', true);
        if (reponse == "A") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question16"] .reponse-a').addClass('reponse-validee-nok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question16"] .quizz-question-reponse').append(q16Faux);
        } else if (reponse == "B") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question16"] .reponse-b').addClass('reponse-validee-nok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question16"] .quizz-question-reponse').append(q16Faux);
        } else if (reponse == "C") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question16"] .reponse-c').addClass('reponse-validee-nok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question16"] .quizz-question-reponse').append(q16Faux);
        } else {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question16"] .reponse-d').addClass('reponse-validee-ok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question16"] .quizz-question-reponse').append(q16Vrai);
        }
        $('#quizz-avc .quizz-custom-wrapper[data-question="question16"] #confirmer-reponse').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question16"] #question-suivante').show();
    });
    // Q16, passage à la question suivante
    $('#quizz-avc .quizz-custom-wrapper[data-question="question16"] #question-suivante').click(function(){
        $('#quizz-avc .quizz-custom-wrapper[data-question="question16"]').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question16"]').removeClass('active');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question17"]').show();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question17"]').addClass('active');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question17"] #question-suivante').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question17"] #confirmer-reponse').prop('disabled', true);
        $('#quizz-avc .quizz-dot-17').addClass('active');
    });


    // Q17, au clic du bouton de validation, vérifie la réponse et affiche le résultat
    $('#quizz-avc .quizz-custom-wrapper[data-question="question17"] #confirmer-reponse').click(function(){
        var reponse = $('#quizz-avc .quizz-question .active button').attr('data-reponse');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question17"] .reponse-a button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question17"] .reponse-b button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question17"] .reponse-c button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question17"] .reponse-d button').prop('disabled', true);
        if (reponse == "A") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question17"] .reponse-a').addClass('reponse-validee-ok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question17"] .quizz-question-reponse').append(q17Vrai);
        } else if (reponse == "B") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question17"] .reponse-b').addClass('reponse-validee-nok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question17"] .quizz-question-reponse').append(q17Faux);
        } else if (reponse == "C") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question17"] .reponse-c').addClass('reponse-validee-nok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question17"] .quizz-question-reponse').append(q17Faux);
        } else {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question17"] .reponse-d').addClass('reponse-validee-nok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question17"] .quizz-question-reponse').append(q17Faux);
        }
        $('#quizz-avc .quizz-custom-wrapper[data-question="question17"] #confirmer-reponse').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question17"] #question-suivante').show();
    });
    // Q17, passage à la question suivante
    $('#quizz-avc .quizz-custom-wrapper[data-question="question17"] #question-suivante').click(function(){
        $('#quizz-avc .quizz-custom-wrapper[data-question="question17"]').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question17"]').removeClass('active');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question18"]').show();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question18"]').addClass('active');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question18"] #question-suivante').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question18"] #confirmer-reponse').prop('disabled', true);
        $('#quizz-avc .quizz-dot-18').addClass('active');
    });


    // Q18, au clic du bouton de validation, vérifie la réponse et affiche le résultat
    $('#quizz-avc .quizz-custom-wrapper[data-question="question18"] #confirmer-reponse').click(function(){
        var reponse = $('#quizz-avc .quizz-question .active button').attr('data-reponse');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question18"] .reponse-a button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question18"] .reponse-b button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question18"] .reponse-c button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question18"] .reponse-d button').prop('disabled', true);
        if (reponse == "A") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question18"] .reponse-a').addClass('reponse-validee-nok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question18"] .quizz-question-reponse').append(q18Faux);
        } else if (reponse == "B") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question18"] .reponse-b').addClass('reponse-validee-nok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question18"] .quizz-question-reponse').append(q18Faux);
        } else if (reponse == "C") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question18"] .reponse-c').addClass('reponse-validee-ok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question18"] .quizz-question-reponse').append(q18Vrai);
        } else {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question18"] .reponse-d').addClass('reponse-validee-ok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question18"] .quizz-question-reponse').append(q18Vrai);
        }
        $('#quizz-avc .quizz-custom-wrapper[data-question="question18"] #confirmer-reponse').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question18"] #question-suivante').show();
    });
    // Q18, passage à la question suivante
    $('#quizz-avc .quizz-custom-wrapper[data-question="question18"] #question-suivante').click(function(){
        $('#quizz-avc .quizz-custom-wrapper[data-question="question18"]').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question18"]').removeClass('active');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question19"]').show();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question19"]').addClass('active');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question19"] #question-suivante').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question19"] #confirmer-reponse').prop('disabled', true);
        $('#quizz-avc .quizz-dot-19').addClass('active');
    });


    // Q19, au clic du bouton de validation, vérifie la réponse et affiche le résultat
    $('#quizz-avc .quizz-custom-wrapper[data-question="question19"] #confirmer-reponse').click(function(){
        var reponse = $('#quizz-avc .quizz-question .active button').attr('data-reponse');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question19"] .reponse-a button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question19"] .reponse-b button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question19"] .reponse-c button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question19"] .reponse-d button').prop('disabled', true);
        if (reponse == "A") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question19"] .reponse-a').addClass('reponse-validee-ok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question19"] .quizz-question-reponse').append(q19Vrai);
        } else if (reponse == "B") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question19"] .reponse-b').addClass('reponse-validee-nok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question19"] .quizz-question-reponse').append(q19Faux);
        } else if (reponse == "C") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question19"] .reponse-c').addClass('reponse-validee-nok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question19"] .quizz-question-reponse').append(q19Faux);
        } else {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question19"] .reponse-d').addClass('reponse-validee-ok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question19"] .quizz-question-reponse').append(q19Vrai);
        }
        $('#quizz-avc .quizz-custom-wrapper[data-question="question19"] #confirmer-reponse').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question19"] #question-suivante').show();
    });
    // Q19, passage à la question suivante
    $('#quizz-avc .quizz-custom-wrapper[data-question="question19"] #question-suivante').click(function(){
        $('#quizz-avc .quizz-custom-wrapper[data-question="question19"]').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question19"]').removeClass('active');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question20"]').show();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question20"]').addClass('active');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question20"] #retour-page-contenu').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question20"] #confirmer-reponse').prop('disabled', true);
        $('#quizz-avc .quizz-dot-20').addClass('active');
    });


    // Q20, au clic du bouton de validation, vérifie la réponse et affiche le résultat
    $('#quizz-avc .quizz-custom-wrapper[data-question="question20"] #confirmer-reponse').click(function(){
        var reponse = $('#quizz-avc .quizz-question .active button').attr('data-reponse');
        $('#quizz-avc .quizz-custom-wrapper[data-question="question20"] .reponse-a button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question20"] .reponse-b button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question20"] .reponse-c button').prop('disabled', true);
        $('#quizz-avc .quizz-custom-wrapper[data-question="question20"] .reponse-d button').prop('disabled', true);
        if (reponse == "A") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question20"] .reponse-a').addClass('reponse-validee-nok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question20"] .quizz-question-reponse').append(q20Faux);
        } else if (reponse == "B") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question20"] .reponse-b').addClass('reponse-validee-ok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question20"] .quizz-question-reponse').append(q20Vrai);
        } else if (reponse == "C") {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question20"] .reponse-c').addClass('reponse-validee-nok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question20"] .quizz-question-reponse').append(q20Faux);
        } else {
            $('#quizz-avc .quizz-custom-wrapper[data-question="question20"] .reponse-d').addClass('reponse-validee-nok');
            $('#quizz-avc .quizz-custom-wrapper[data-question="question20"] .quizz-question-reponse').append(q20Faux);
        }
        $('#quizz-avc .quizz-custom-wrapper[data-question="question20"] #confirmer-reponse').hide();
        $('#quizz-avc .quizz-custom-wrapper[data-question="question20"] #retour-page-contenu').show();
    });
    // Q20, retour au contenu de la page initiale et réinitialisation du quizz (Sans rechargement de page)
    $('#quizz-avc .quizz-custom-wrapper[data-question="question20"] #retour-page-contenu').click(function(){
        location.reload();
    });



})(jQuery, Drupal);