(function ($, Drupal) {

    'use strict';
  
    /* Gestion formulaire FA */
    // Base réponses
    var qStartVrai = '<p class="titre-rep-ok">Exact !</p><p class="texte-rep-ok">';
    var qStartFaux = '<p class="titre-rep-nok">Mauvaise Réponse !</p><p class="texte-rep-ok">';
    // Réponse Q1
    var q1Rep = 'Réponses B et C :</p><p>Le vrai nom est Fibrillation Atriale (FA) mais les termes "fibrillation auriculaire" ou "arythmie cardiaque" sont également utilisés. Le coeur s’accélère : c’est, ce que l\'on appelle" la tachycardie". Cette tachycardie entraîne une accélération irrégulière du ventricule, d’où le terme d’arythmie.</p>';
    var q1Vrai = qStartVrai+q1Rep;
    var q1Faux = qStartFaux+q1Rep;
    // Réponse Q2
    var q2Rep = 'Réponse C :</p><p>Cette maladie peut survenir suite à un problème cardiaque, comme sur un coeur sain.</p><p>La FA survient :</p><p>- Soit sur coeur sain, en l’absence de maladie cardiaque,<br />- Soit chez les sujets âgés (fatigue de l’oreillette)<br />- Soit suite à un problème cardiaque (insuffisance cardiaque par exemple)<br />- Soit à cause d’autres facteurs (hyperthyroïdie, alcool, …)</p>';
    var q2Vrai = qStartVrai+q2Rep;
    var q2Faux = qStartFaux+q2Rep;
    // Réponse Q3
    var q3Rep = 'Réponses A, B et C :</p><p>Les principaux symptômes d\'une FA sont la fatigue, l\'essoufflement, des palpitations, des douleurs thoraciques, une instabilité, des vertiges…</p><p>Certaines personnes souffrant de FA peuvent présenter régulièrement des symptômes tandis que d’autres n’en présentent que rarement, voire jamais. On estime que pour 1 patient sur 3, cette maladie ne présente aucun symptôme.</p>';
    var q3Vrai = qStartVrai+q3Rep;
    var q3Faux = qStartFaux+q3Rep;
    // Réponse Q4
    var q4Rep = 'Réponses A et D :</p><p>Le coeur bat rapidement et de manière irrégulière, les oreillettes qui commandent les battements du coeur se contractent de façon rapide, irrégulière et désordonnée et ne fonctionnent plus correctement. Le sang peut alors stagner et former des caillots. Le caillot ou une partie du caillot peut se détacher et se déplacer vers les artères cérébrales, ce qui peut provoquer un accident vasculaire cérébral (AVC).</p>';
    var q4Vrai = qStartVrai+q4Rep;
    var q4Faux = qStartFaux+q4Rep;
    // Réponse Q5
    var q5Rep = 'Réponses A, B, C et D :</p><p>Lentement ou rapidement, irrégulièrement ou régulièrement ou les 2 alternativement.</p><p>La FA peut être permanente (c’est-à-dire que le coeur bat irrégulièrement en permanence) ou paroxystique (c’est-à-dire que le coeur bat irrégulièrement de temps en temps). Cette FA paroxystique peut évoluer vers une FA permanente. Que le coeur batte régulièrement ou non, le traitement par anticoagulant sera le même.</p>';
    var q5Vrai = qStartVrai+q5Rep;
    var q5Faux = qStartFaux+q5Rep;
    // Réponse Q6
    var q6Rep = 'Réponses B et D :</p><p>Le risque principal est la survenue d’un Accident Vasculaire Cérébral (AVC). L’AVC peut avoir des conséquences physiques importantes (paralysie, difficultés à marcher, à parler, etc…).</p><p>L’oreillette se contracte mal, le sang stagne. Il peut alors se former un caillot qui peut interrompre la circulation sanguine et/ou se déplacer et boucher une artère cérébrale. Ceci peut provoquer un AVC entraînant de lourdes séquelles.</p>';
    var q6Vrai = qStartVrai+q6Rep;
    var q6Faux = qStartFaux+q6Rep;
    // Réponse Q7
    var q7Rep = 'Réponse C :</p><p>Afin d’éviter la formation de ces caillots, il faut fluidifier le sang. C’est pour cela que le médecin prescrit un traitement anticoagulant et qu\'il est très important de le suivre dans la durée.</p>';
    var q7Vrai = qStartVrai+q7Rep;
    var q7Faux = qStartFaux+q7Rep;
    // Réponse Q8
    var q8Rep = 'Réponses A, B et D :</p><p>Un traitement anticoagulant est un médicament qui fluidifie le sang et qui empêche la formation de caillots sanguins.</p><p>Il existe plusieurs types d’anticoagulants :</p><p>- Les antivitamine K (AVK) sont des médicaments qui fluidifient le sang. Les AVK nécessitent une surveillance en routine par prise de sang (INR),<br />- Les anticoagulants oraux directs (AOD) agissent directement dans le sang sur les facteurs de coagulation. Ils ne nécessitent pas de surveillance en routine par prise de sang.</p>';
    var q8Vrai = qStartVrai+q8Rep;
    var q8Faux = qStartFaux+q8Rep;
    // Réponse Q9
    var q9Rep = 'Réponse B :</p><p>Son rôle est d’empêcher la formation d’un caillot sanguin (ou thrombus).</p><p>La formation puis la migration de ce caillot dans la circulation sanguine peut entraîner un AVC (risque le plus fréquent de la FA). La FA multiplie par 5 le risque d’AVC.</p>';
    var q9Vrai = qStartVrai+q9Rep;
    var q9Faux = qStartFaux+q9Rep;
    // Réponse Q10
    var q10Rep = 'Réponse A :</p><p>Selon le traitement, les modalités de prise peuvent être différentes :</p><p>- AVK : une prise unique, le soir de préférence. La dose doit être adaptée par le médecin en fonction de l’INR (prises de sang régulières).<br />- Anticoagulants Oraux Directs (AOD) : selon le traitement anticoagulant prescrit par le médecin :<br />o Une à deux prises par jour,<br />o A prendre pendant ou en dehors des repas (se reporter à la notice).<br />La dose est fixe.</p><p>Veuillez vous référer à votre ordonnance. Pour plus d\'informations, vous pouvez également consulter la notice du médicament.</p>';
    var q10Vrai = qStartVrai+q10Rep;
    var q10Faux = qStartFaux+q10Rep;
    // Réponse Q11
    var q11Rep = 'Réponse D :</p><p>Le traitement anticoagulant doit être pris :</p><p>- Tous les jours<br />- A heures fixes<br />- Pendant ou en dehors des repas, selon le traitement anticoagulant*<br /></p><p>Il ne faut jamais modifier la dose ou arrêter le traitement sans l’avis du médecin ou du pharmacien. Il faut vérifier les éventuelles contre-indications, intéractions médicamenteuses et autres précautions d’emploi (se reporter à la notice du médicament et toujours suivre les conseils du médecin et/ou du pharmacien).</p><p>Certains traitements anticoagulants exigent une surveillance biologique régulière avec des prises de sang. Il ne faut pas hésiter à demander conseil au médecin ou au pharmacien.</p>';
    var q11Vrai = qStartVrai+q11Rep;
    var q11Faux = qStartFaux+q11Rep;
    // Réponse Q12
    var q12Rep = 'Aucune réponse :</p><p>NON ! L\'arrêt est possible uniquement après avis médical.</p><p>- En cas de saignement inhabituel (dans les urines ou les selles, lors du brossage de dents, lors d\'un saignement de nez ou lors de vomissement), il faut consulter rapidement un médecin pour la conduite à tenir (prise en charge au cas par cas).<br />- L’arrêt du traitement anticoagulant avant une intervention invasive (fibroscopie, coelioscopie, coloscopie, infiltration…) ou chirurgicale peut-être envisagé après avis médical et sera discuté avec le cardiologue, l’anesthésiste et le praticien qui opère (chirurgien, dentiste…).<br />- En cas de survenue de saignement, un traitement en relais peut-être discuté et mis en place si nécessaire.</p><p>Dans tous les cas, la conduite à tenir sera définie avec le médecin.</p>';
    var q12Vrai = qStartVrai+q12Rep;
    var q12Faux = qStartFaux+q12Rep;
    // Réponse Q13
    var q13Rep = 'Réponses C et D :</p><p>Certains anticoagulants se prennent en une prise par jour, d’autres en deux prises. En cas d’oubli, la marche à suivre peut être différente d’un médicament à l’autre (se reporter à la notice).</p>';
    var q13Vrai = qStartVrai+q13Rep;
    var q13Faux = qStartFaux+q13Rep;
    // Réponse Q14
    var q14Rep = 'Réponses A, B et C :</p><p>Voici quelques astuces pour ne pas oublier de prendre son traitement anticoagulant :</p><p>- Utiliser un semainier ou un pilulier<br />- Associer la prise du traitement à un geste quotidien (en respectant les modalités de prise du traitement)<br />- Mettre une alarme sur le téléphone portable<br />- Ne pas oublier de renouveler l’ordonnance avant la fin du traitement pour ne pas l’interrompre</p>';
    var q14Vrai = qStartVrai+q14Rep;
    var q14Faux = qStartFaux+q14Rep;
    // Réponse Q15
    var q15Rep = 'Aucune réponse :</p><p>La conduite à tenir est variable en fonction du médicament administré. En cas de vomissement, il faut contacter le médecin ou pharmacien pour connaître la conduite à tenir.</p><p>En fonction du médicament, la conduite à tenir sera définie par le professionnel de santé.</p>';
    var q15Vrai = qStartVrai+q15Rep;
    var q15Faux = qStartFaux+q15Rep;
    // Réponse Q16
    var q16Rep = 'Réponses A et D :</p><p>Il faut être prudent pour éviter les chutes : (car le fait d\'être sous anticoagulant aura tendance à vous faire plus saigner (plaie ouverte et/ou hématome)</p><p>- Eviter l’encombrement au sol,<br />- Essuyer tout ce qui pourrait rendre le sol glissant,<br />- Mettre un tapis antidérapant dans la douche.<br /></p><p>Il faut bien choisir ses activités sportives :</p><p>- Eviter les sports de combat, le rugby,<br />- Limiter le VTT et le ski,<br />- Privilégier la natation, la marche (car il y a moins de risque de tomber et/ou de se blesser).</p>';
    var q16Vrai = qStartVrai+q16Rep;
    var q16Faux = qStartFaux+q16Rep;
    // Réponse Q17
    var q17Rep = 'Réponses A, B et C :</p><p>Il faut informer :</p><p>- L\'entourage proche,<br />- Le corps médical (médecin traitant, cardiologue, rhumatologue, dentiste, infirmière…)<br />- Les urgentistes en cas d’accident ou d’hospitalisation.<br /></p><p>Toutes ces personnes doivent être informées de la prise d\'anticoagulant afin de connaître la conduite à tenir en cas de saignement. Une carte patient « Je suis sous anticoagulant » doit être remise par le médecin.</p>';
    var q17Vrai = qStartVrai+q17Rep;
    var q17Faux = qStartFaux+q17Rep;
    // Réponse Q18
    var q18Rep = 'Réponses C et D :</p><p>Il est important de porter sur soi avec les papiers d’identité une carte « Patient sous Anticoagulant ».</p><p>Elle sert à informer les équipes soignantes :</p><p>- En cas d’inconscience lors d\'un accident nécessitant des soins (exemple accident de la voie publique, etc)<br />- Pour adapter les soins en fonction du type de molécule<br />- Dans le cadre de consultations réglées avec un nouvel intervenant, cela évite le risque de prescription de médicaments susceptibles d’induire des interactions</p>';
    var q18Vrai = qStartVrai+q18Rep;
    var q18Faux = qStartFaux+q18Rep;
    // Réponse Q19
    var q19Rep = 'Réponses A, B, C et D :</p><p>Il faut surveiller tout saignement inhabituel :</p><p>- Lors du brossage les dents,<br />- Lors d\'un saignement de nez,<br />- Dans les urines ou les selles,<br />- Lors de vomissements.</p>';
    var q19Vrai = qStartVrai+q19Rep;
    var q19Faux = qStartFaux+q19Rep;
    // Réponse Q20
    var q20Rep = 'Réponses A, C et D :</p><p>En cas de modification de l\'état général, il faut informer le médecin ou le pharmacien. Il faut être notamment attentif à :</p><p>- La fatigue<br />- La pâleur<br />- L\'essoufflement<br />- Aux malaises<br />- Aux maux de tête (persistant malgré la prise d’un traitement antalgique)<br />- A la présence de douleur, gonflement</p>';
    var q20Vrai = qStartVrai+q20Rep;
    var q20Faux = qStartFaux+q20Rep;




    // Q1, au clic du bouton de validation, vérifie la réponse et affiche le résultat
    $('#quizz-fa .quizz-custom-wrapper[data-question="question1"] #confirmer-reponse').click(function(){
        var reponse = $('#quizz-fa .quizz-question .active button').attr('data-reponse');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question1"] .reponse-a button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question1"] .reponse-b button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question1"] .reponse-c button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question1"] .reponse-d button').prop('disabled', true);
        if (reponse == "A") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question1"] .reponse-a').addClass('reponse-validee-nok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question1"] .quizz-question-reponse').append(q1Faux);
        } else if (reponse == "B") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question1"] .reponse-b').addClass('reponse-validee-ok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question1"] .quizz-question-reponse').append(q1Vrai);
        } else if (reponse == "C") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question1"] .reponse-c').addClass('reponse-validee-ok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question1"] .quizz-question-reponse').append(q1Vrai);
        } else {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question1"] .reponse-d').addClass('reponse-validee-nok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question1"] .quizz-question-reponse').append(q1Faux);
        }
        $('#quizz-fa .quizz-custom-wrapper[data-question="question1"] #confirmer-reponse').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question1"] #question-suivante').show();
    });
    // Q1, passage à la question suivante
    $('#quizz-fa .quizz-custom-wrapper[data-question="question1"] #question-suivante').click(function(){
        $('#quizz-fa .quizz-custom-wrapper[data-question="question1"]').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question1"]').removeClass('active');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question2"]').show();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question2"]').addClass('active');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question2"] #question-suivante').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question2"] #confirmer-reponse').prop('disabled', true);
        $('#quizz-fa .quizz-dot-2').addClass('active');
    });


    // Q2, au clic du bouton de validation, vérifie la réponse et affiche le résultat
    $('#quizz-fa .quizz-custom-wrapper[data-question="question2"] #confirmer-reponse').click(function(){
        var reponse = $('#quizz-fa .quizz-question .active button').attr('data-reponse');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question2"] .reponse-a button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question2"] .reponse-b button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question2"] .reponse-c button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question2"] .reponse-d button').prop('disabled', true);
        if (reponse == "A") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question2"] .reponse-a').addClass('reponse-validee-nok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question2"] .quizz-question-reponse').append(q2Faux);
        } else if (reponse == "B") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question2"] .reponse-b').addClass('reponse-validee-nok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question2"] .quizz-question-reponse').append(q2Faux);
        } else if (reponse == "C") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question2"] .reponse-c').addClass('reponse-validee-ok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question2"] .quizz-question-reponse').append(q2Vrai);
        } else {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question2"] .reponse-d').addClass('reponse-validee-nok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question2"] .quizz-question-reponse').append(q2Faux);
        }
        $('#quizz-fa .quizz-custom-wrapper[data-question="question2"] #confirmer-reponse').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question2"] #question-suivante').show();
    });
    // Q2, passage à la question suivante
    $('#quizz-fa .quizz-custom-wrapper[data-question="question2"] #question-suivante').click(function(){
        $('#quizz-fa .quizz-custom-wrapper[data-question="question2"]').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question2"]').removeClass('active');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question3"]').show();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question3"]').addClass('active');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question3"] #question-suivante').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question3"] #confirmer-reponse').prop('disabled', true);
        $('#quizz-fa .quizz-dot-3').addClass('active');
    });


    // Q3, au clic du bouton de validation, vérifie la réponse et affiche le résultat
    $('#quizz-fa .quizz-custom-wrapper[data-question="question3"] #confirmer-reponse').click(function(){
        var reponse = $('#quizz-fa .quizz-question .active button').attr('data-reponse');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question3"] .reponse-a button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question3"] .reponse-b button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question3"] .reponse-c button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question3"] .reponse-d button').prop('disabled', true);
        if (reponse == "A") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question3"] .reponse-a').addClass('reponse-validee-ok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question3"] .quizz-question-reponse').append(q3Vrai);
        } else if (reponse == "B") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question3"] .reponse-b').addClass('reponse-validee-ok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question3"] .quizz-question-reponse').append(q3Vrai);
        } else if (reponse == "C") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question3"] .reponse-c').addClass('reponse-validee-ok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question3"] .quizz-question-reponse').append(q3Vrai);
        } else {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question3"] .reponse-d').addClass('reponse-validee-nok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question3"] .quizz-question-reponse').append(q3Faux);
        }
        $('#quizz-fa .quizz-custom-wrapper[data-question="question3"] #confirmer-reponse').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question3"] #question-suivante').show();
    });
    // Q3, passage à la question suivante
    $('#quizz-fa .quizz-custom-wrapper[data-question="question3"] #question-suivante').click(function(){
        $('#quizz-fa .quizz-custom-wrapper[data-question="question3"]').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question3"]').removeClass('active');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question4"]').show();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question4"]').addClass('active');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question4"] #question-suivante').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question4"] #confirmer-reponse').prop('disabled', true);
        $('#quizz-fa .quizz-dot-4').addClass('active');
    });


    // Q4, au clic du bouton de validation, vérifie la réponse et affiche le résultat
    $('#quizz-fa .quizz-custom-wrapper[data-question="question4"] #confirmer-reponse').click(function(){
        var reponse = $('#quizz-fa .quizz-question .active button').attr('data-reponse');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question4"] .reponse-a button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question4"] .reponse-b button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question4"] .reponse-c button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question4"] .reponse-d button').prop('disabled', true);
        if (reponse == "A") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question4"] .reponse-a').addClass('reponse-validee-ok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question4"] .quizz-question-reponse').append(q4Vrai);
        } else if (reponse == "B") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question4"] .reponse-b').addClass('reponse-validee-nok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question4"] .quizz-question-reponse').append(q4Faux);
        } else if (reponse == "C") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question4"] .reponse-c').addClass('reponse-validee-nok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question4"] .quizz-question-reponse').append(q4Faux);
        } else {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question4"] .reponse-d').addClass('reponse-validee-ok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question4"] .quizz-question-reponse').append(q4Vrai);
        }
        $('#quizz-fa .quizz-custom-wrapper[data-question="question4"] #confirmer-reponse').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question4"] #question-suivante').show();
    });
    // Q4, passage à la question suivante
    $('#quizz-fa .quizz-custom-wrapper[data-question="question4"] #question-suivante').click(function(){
        $('#quizz-fa .quizz-custom-wrapper[data-question="question4"]').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question4"]').removeClass('active');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question5"]').show();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question5"]').addClass('active');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question5"] #question-suivante').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question5"] #confirmer-reponse').prop('disabled', true);
        $('#quizz-fa .quizz-dot-5').addClass('active');
    });


    // Q5, au clic du bouton de validation, vérifie la réponse et affiche le résultat
    $('#quizz-fa .quizz-custom-wrapper[data-question="question5"] #confirmer-reponse').click(function(){
        var reponse = $('#quizz-fa .quizz-question .active button').attr('data-reponse');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question5"] .reponse-a button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question5"] .reponse-b button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question5"] .reponse-c button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question5"] .reponse-d button').prop('disabled', true);
        if (reponse == "A") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question5"] .reponse-a').addClass('reponse-validee-ok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question5"] .quizz-question-reponse').append(q5Vrai);
        } else if (reponse == "B") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question5"] .reponse-b').addClass('reponse-validee-ok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question5"] .quizz-question-reponse').append(q5Vrai);
        } else if (reponse == "C") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question5"] .reponse-c').addClass('reponse-validee-ok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question5"] .quizz-question-reponse').append(q5Vrai);
        } else {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question5"] .reponse-d').addClass('reponse-validee-ok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question5"] .quizz-question-reponse').append(q5Vrai);
        }
        $('#quizz-fa .quizz-custom-wrapper[data-question="question5"] #confirmer-reponse').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question5"] #question-suivante').show();
    });
    // Q5, passage à la question suivante
    $('#quizz-fa .quizz-custom-wrapper[data-question="question5"] #question-suivante').click(function(){
        $('#quizz-fa .quizz-custom-wrapper[data-question="question5"]').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question5"]').removeClass('active');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question6"]').show();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question6"]').addClass('active');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question6"] #question-suivante').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question6"] #confirmer-reponse').prop('disabled', true);
        $('#quizz-fa .quizz-dot-6').addClass('active');
    });


    // Q6, au clic du bouton de validation, vérifie la réponse et affiche le résultat
    $('#quizz-fa .quizz-custom-wrapper[data-question="question6"] #confirmer-reponse').click(function(){
        var reponse = $('#quizz-fa .quizz-question .active button').attr('data-reponse');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question6"] .reponse-a button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question6"] .reponse-b button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question6"] .reponse-c button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question6"] .reponse-d button').prop('disabled', true);
        if (reponse == "A") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question6"] .reponse-a').addClass('reponse-validee-nok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question6"] .quizz-question-reponse').append(q6Faux);
        } else if (reponse == "B") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question6"] .reponse-b').addClass('reponse-validee-ok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question6"] .quizz-question-reponse').append(q6Vrai);
        } else if (reponse == "C") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question6"] .reponse-c').addClass('reponse-validee-nok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question6"] .quizz-question-reponse').append(q6Faux);
        } else {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question6"] .reponse-d').addClass('reponse-validee-ok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question6"] .quizz-question-reponse').append(q6Vrai);
        }
        $('#quizz-fa .quizz-custom-wrapper[data-question="question6"] #confirmer-reponse').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question6"] #question-suivante').show();
    });
    // Q6, passage à la question suivante
    $('#quizz-fa .quizz-custom-wrapper[data-question="question6"] #question-suivante').click(function(){
        $('#quizz-fa .quizz-custom-wrapper[data-question="question6"]').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question6"]').removeClass('active');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question7"]').show();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question7"]').addClass('active');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question7"] #question-suivante').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question7"] #confirmer-reponse').prop('disabled', true);
        $('#quizz-fa .quizz-dot-7').addClass('active');
    });


    // Q7, au clic du bouton de validation, vérifie la réponse et affiche le résultat
    $('#quizz-fa .quizz-custom-wrapper[data-question="question7"] #confirmer-reponse').click(function(){
        var reponse = $('#quizz-fa .quizz-question .active button').attr('data-reponse');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question7"] .reponse-a button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question7"] .reponse-b button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question7"] .reponse-c button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question7"] .reponse-d button').prop('disabled', true);
        if (reponse == "A") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question7"] .reponse-a').addClass('reponse-validee-nok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question7"] .quizz-question-reponse').append(q7Faux);
        } else if (reponse == "B") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question7"] .reponse-b').addClass('reponse-validee-nok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question7"] .quizz-question-reponse').append(q7Faux);
        } else if (reponse == "C") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question7"] .reponse-c').addClass('reponse-validee-ok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question7"] .quizz-question-reponse').append(q7Vrai);
        } else {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question7"] .reponse-d').addClass('reponse-validee-nok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question7"] .quizz-question-reponse').append(q7Faux);
        }
        $('#quizz-fa .quizz-custom-wrapper[data-question="question7"] #confirmer-reponse').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question7"] #question-suivante').show();
    });
    // Q7, passage à la question suivante
    $('#quizz-fa .quizz-custom-wrapper[data-question="question7"] #question-suivante').click(function(){
        $('#quizz-fa .quizz-custom-wrapper[data-question="question7"]').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question7"]').removeClass('active');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question8"]').show();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question8"]').addClass('active');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question8"] #question-suivante').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question8"] #confirmer-reponse').prop('disabled', true);
        $('#quizz-fa .quizz-dot-8').addClass('active');
    });


    // Q8, au clic du bouton de validation, vérifie la réponse et affiche le résultat
    $('#quizz-fa .quizz-custom-wrapper[data-question="question8"] #confirmer-reponse').click(function(){
        var reponse = $('#quizz-fa .quizz-question .active button').attr('data-reponse');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question8"] .reponse-a button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question8"] .reponse-b button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question8"] .reponse-c button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question8"] .reponse-d button').prop('disabled', true);
        if (reponse == "A") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question8"] .reponse-a').addClass('reponse-validee-ok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question8"] .quizz-question-reponse').append(q8Vrai);
        } else if (reponse == "B") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question8"] .reponse-b').addClass('reponse-validee-ok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question8"] .quizz-question-reponse').append(q8Vrai);
        } else if (reponse == "C") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question8"] .reponse-c').addClass('reponse-validee-nok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question8"] .quizz-question-reponse').append(q8Faux);
        } else {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question8"] .reponse-d').addClass('reponse-validee-ok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question8"] .quizz-question-reponse').append(q8Vrai);
        }
        $('#quizz-fa .quizz-custom-wrapper[data-question="question8"] #confirmer-reponse').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question8"] #question-suivante').show();
    });
    // Q8, passage à la question suivante
    $('#quizz-fa .quizz-custom-wrapper[data-question="question8"] #question-suivante').click(function(){
        $('#quizz-fa .quizz-custom-wrapper[data-question="question8"]').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question8"]').removeClass('active');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question9"]').show();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question9"]').addClass('active');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question9"] #question-suivante').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question9"] #confirmer-reponse').prop('disabled', true);
        $('#quizz-fa .quizz-dot-9').addClass('active');
    });


    // Q9, au clic du bouton de validation, vérifie la réponse et affiche le résultat
    $('#quizz-fa .quizz-custom-wrapper[data-question="question9"] #confirmer-reponse').click(function(){
        var reponse = $('#quizz-fa .quizz-question .active button').attr('data-reponse');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question9"] .reponse-a button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question9"] .reponse-b button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question9"] .reponse-c button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question9"] .reponse-d button').prop('disabled', true);
        if (reponse == "A") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question9"] .reponse-a').addClass('reponse-validee-nok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question9"] .quizz-question-reponse').append(q9Faux);
        } else if (reponse == "B") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question9"] .reponse-b').addClass('reponse-validee-ok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question9"] .quizz-question-reponse').append(q9Vrai);
        } else if (reponse == "C") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question9"] .reponse-c').addClass('reponse-validee-nok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question9"] .quizz-question-reponse').append(q9Faux);
        } else {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question9"] .reponse-d').addClass('reponse-validee-nok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question9"] .quizz-question-reponse').append(q9Faux);
        }
        $('#quizz-fa .quizz-custom-wrapper[data-question="question9"] #confirmer-reponse').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question9"] #question-suivante').show();
    });
    // Q9, passage à la question suivante
    $('#quizz-fa .quizz-custom-wrapper[data-question="question9"] #question-suivante').click(function(){
        $('#quizz-fa .quizz-custom-wrapper[data-question="question9"]').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question9"]').removeClass('active');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question10"]').show();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question10"]').addClass('active');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question10"] #question-suivante').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question10"] #confirmer-reponse').prop('disabled', true);
        $('#quizz-fa .quizz-dot-10').addClass('active');
    });


    // Q10, au clic du bouton de validation, vérifie la réponse et affiche le résultat
    $('#quizz-fa .quizz-custom-wrapper[data-question="question10"] #confirmer-reponse').click(function(){
        var reponse = $('#quizz-fa .quizz-question .active button').attr('data-reponse');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question10"] .reponse-a button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question10"] .reponse-b button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question10"] .reponse-c button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question10"] .reponse-d button').prop('disabled', true);
        if (reponse == "A") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question10"] .reponse-a').addClass('reponse-validee-ok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question10"] .quizz-question-reponse').append(q10Vrai);
        } else if (reponse == "B") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question10"] .reponse-b').addClass('reponse-validee-nok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question10"] .quizz-question-reponse').append(q10Faux);
        } else if (reponse == "C") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question10"] .reponse-c').addClass('reponse-validee-nok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question10"] .quizz-question-reponse').append(q10Faux);
        } else {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question10"] .reponse-d').addClass('reponse-validee-nok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question10"] .quizz-question-reponse').append(q10Faux);
        }
        $('#quizz-fa .quizz-custom-wrapper[data-question="question10"] #confirmer-reponse').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question10"] #question-suivante').show();
    });
    // Q10, passage à la question suivante
    $('#quizz-fa .quizz-custom-wrapper[data-question="question10"] #question-suivante').click(function(){
        $('#quizz-fa .quizz-custom-wrapper[data-question="question10"]').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question10"]').removeClass('active');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question11"]').show();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question11"]').addClass('active');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question11"] #question-suivante').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question11"] #confirmer-reponse').prop('disabled', true);
        $('#quizz-fa .quizz-dot-11').addClass('active');
    });


    // Q11, au clic du bouton de validation, vérifie la réponse et affiche le résultat
    $('#quizz-fa .quizz-custom-wrapper[data-question="question11"] #confirmer-reponse').click(function(){
        var reponse = $('#quizz-fa .quizz-question .active button').attr('data-reponse');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question11"] .reponse-a button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question11"] .reponse-b button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question11"] .reponse-c button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question11"] .reponse-d button').prop('disabled', true);
        if (reponse == "A") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question11"] .reponse-a').addClass('reponse-validee-nok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question11"] .quizz-question-reponse').append(q11Faux);
        } else if (reponse == "B") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question11"] .reponse-b').addClass('reponse-validee-nok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question11"] .quizz-question-reponse').append(q11Faux);
        } else if (reponse == "C") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question11"] .reponse-c').addClass('reponse-validee-nok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question11"] .quizz-question-reponse').append(q11Faux);
        } else {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question11"] .reponse-d').addClass('reponse-validee-ok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question11"] .quizz-question-reponse').append(q11Vrai);
        }
        $('#quizz-fa .quizz-custom-wrapper[data-question="question11"] #confirmer-reponse').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question11"] #question-suivante').show();
    });
    // Q11, passage à la question suivante
    $('#quizz-fa .quizz-custom-wrapper[data-question="question11"] #question-suivante').click(function(){
        $('#quizz-fa .quizz-custom-wrapper[data-question="question11"]').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question11"]').removeClass('active');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question12"]').show();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question12"]').addClass('active');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question12"] #question-suivante').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question12"] #confirmer-reponse').prop('disabled', true);
        $('#quizz-fa .quizz-dot-12').addClass('active');
    });


    // Q12, au clic du bouton de validation, vérifie la réponse et affiche le résultat
    $('#quizz-fa .quizz-custom-wrapper[data-question="question12"] #confirmer-reponse').click(function(){
        var reponse = $('#quizz-fa .quizz-question .active button').attr('data-reponse');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question12"] .reponse-a button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question12"] .reponse-b button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question12"] .reponse-c button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question12"] .reponse-d button').prop('disabled', true);
        if (reponse == "A") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question12"] .reponse-a').addClass('reponse-validee-nok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question12"] .quizz-question-reponse').append(q12Faux);
        } else if (reponse == "B") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question12"] .reponse-b').addClass('reponse-validee-nok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question12"] .quizz-question-reponse').append(q12Faux);
        } else if (reponse == "C") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question12"] .reponse-c').addClass('reponse-validee-nok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question12"] .quizz-question-reponse').append(q12Faux);
        } else {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question12"] .reponse-d').addClass('reponse-validee-nok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question12"] .quizz-question-reponse').append(q12Faux);
        }
        $('#quizz-fa .quizz-custom-wrapper[data-question="question12"] #confirmer-reponse').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question12"] #question-suivante').show();
    });
    // Q12, passage à la question suivante
    $('#quizz-fa .quizz-custom-wrapper[data-question="question12"] #question-suivante').click(function(){
        $('#quizz-fa .quizz-custom-wrapper[data-question="question12"]').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question12"]').removeClass('active');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question13"]').show();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question13"]').addClass('active');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question13"] #question-suivante').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question13"] #confirmer-reponse').prop('disabled', true);
        $('#quizz-fa .quizz-dot-13').addClass('active');
    });


    // Q13, au clic du bouton de validation, vérifie la réponse et affiche le résultat
    $('#quizz-fa .quizz-custom-wrapper[data-question="question13"] #confirmer-reponse').click(function(){
        var reponse = $('#quizz-fa .quizz-question .active button').attr('data-reponse');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question13"] .reponse-a button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question13"] .reponse-b button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question13"] .reponse-c button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question13"] .reponse-d button').prop('disabled', true);
        if (reponse == "A") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question13"] .reponse-a').addClass('reponse-validee-nok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question13"] .quizz-question-reponse').append(q13Faux);
        } else if (reponse == "B") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question13"] .reponse-b').addClass('reponse-validee-nok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question13"] .quizz-question-reponse').append(q13Faux);
        } else if (reponse == "C") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question13"] .reponse-c').addClass('reponse-validee-ok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question13"] .quizz-question-reponse').append(q13Vrai);
        } else {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question13"] .reponse-d').addClass('reponse-validee-ok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question13"] .quizz-question-reponse').append(q13Vrai);
        }
        $('#quizz-fa .quizz-custom-wrapper[data-question="question13"] #confirmer-reponse').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question13"] #question-suivante').show();
    });
    // Q13, passage à la question suivante
    $('#quizz-fa .quizz-custom-wrapper[data-question="question13"] #question-suivante').click(function(){
        $('#quizz-fa .quizz-custom-wrapper[data-question="question13"]').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question13"]').removeClass('active');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question14"]').show();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question14"]').addClass('active');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question14"] #question-suivante').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question14"] #confirmer-reponse').prop('disabled', true);
        $('#quizz-fa .quizz-dot-14').addClass('active');
    });


    // Q14, au clic du bouton de validation, vérifie la réponse et affiche le résultat
    $('#quizz-fa .quizz-custom-wrapper[data-question="question14"] #confirmer-reponse').click(function(){
        var reponse = $('#quizz-fa .quizz-question .active button').attr('data-reponse');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question14"] .reponse-a button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question14"] .reponse-b button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question14"] .reponse-c button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question14"] .reponse-d button').prop('disabled', true);
        if (reponse == "A") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question14"] .reponse-a').addClass('reponse-validee-ok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question14"] .quizz-question-reponse').append(q14Vrai);
        } else if (reponse == "B") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question14"] .reponse-b').addClass('reponse-validee-ok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question14"] .quizz-question-reponse').append(q14Vrai);
        } else if (reponse == "C") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question14"] .reponse-c').addClass('reponse-validee-ok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question14"] .quizz-question-reponse').append(q14Vrai);
        } else {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question14"] .reponse-d').addClass('reponse-validee-nok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question14"] .quizz-question-reponse').append(q14Faux);
        }
        $('#quizz-fa .quizz-custom-wrapper[data-question="question14"] #confirmer-reponse').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question14"] #question-suivante').show();
    });
    // Q14, passage à la question suivante
    $('#quizz-fa .quizz-custom-wrapper[data-question="question14"] #question-suivante').click(function(){
        $('#quizz-fa .quizz-custom-wrapper[data-question="question14"]').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question14"]').removeClass('active');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question15"]').show();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question15"]').addClass('active');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question15"] #question-suivante').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question15"] #confirmer-reponse').prop('disabled', true);
        $('#quizz-fa .quizz-dot-15').addClass('active');
    });


    // Q15, au clic du bouton de validation, vérifie la réponse et affiche le résultat
    $('#quizz-fa .quizz-custom-wrapper[data-question="question15"] #confirmer-reponse').click(function(){
        var reponse = $('#quizz-fa .quizz-question .active button').attr('data-reponse');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question15"] .reponse-a button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question15"] .reponse-b button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question15"] .reponse-c button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question15"] .reponse-d button').prop('disabled', true);
        if (reponse == "A") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question15"] .reponse-a').addClass('reponse-validee-nok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question15"] .quizz-question-reponse').append(q15Faux);
        } else if (reponse == "B") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question15"] .reponse-b').addClass('reponse-validee-nok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question15"] .quizz-question-reponse').append(q15Faux);
        } else if (reponse == "C") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question15"] .reponse-c').addClass('reponse-validee-nok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question15"] .quizz-question-reponse').append(q15Faux);
        } else {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question15"] .reponse-d').addClass('reponse-validee-nok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question15"] .quizz-question-reponse').append(q15Faux);
        }
        $('#quizz-fa .quizz-custom-wrapper[data-question="question15"] #confirmer-reponse').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question15"] #question-suivante').show();
    });
    // Q15, passage à la question suivante
    $('#quizz-fa .quizz-custom-wrapper[data-question="question15"] #question-suivante').click(function(){
        $('#quizz-fa .quizz-custom-wrapper[data-question="question15"]').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question15"]').removeClass('active');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question16"]').show();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question16"]').addClass('active');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question16"] #question-suivante').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question16"] #confirmer-reponse').prop('disabled', true);
        $('#quizz-fa .quizz-dot-16').addClass('active');
    });


    // Q16, au clic du bouton de validation, vérifie la réponse et affiche le résultat
    $('#quizz-fa .quizz-custom-wrapper[data-question="question16"] #confirmer-reponse').click(function(){
        var reponse = $('#quizz-fa .quizz-question .active button').attr('data-reponse');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question16"] .reponse-a button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question16"] .reponse-b button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question16"] .reponse-c button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question16"] .reponse-d button').prop('disabled', true);
        if (reponse == "A") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question16"] .reponse-a').addClass('reponse-validee-ok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question16"] .quizz-question-reponse').append(q16Vrai);
        } else if (reponse == "B") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question16"] .reponse-b').addClass('reponse-validee-nok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question16"] .quizz-question-reponse').append(q16Faux);
        } else if (reponse == "C") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question16"] .reponse-c').addClass('reponse-validee-nok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question16"] .quizz-question-reponse').append(q16Faux);
        } else {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question16"] .reponse-d').addClass('reponse-validee-ok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question16"] .quizz-question-reponse').append(q16Vrai);
        }
        $('#quizz-fa .quizz-custom-wrapper[data-question="question16"] #confirmer-reponse').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question16"] #question-suivante').show();
    });
    // Q16, passage à la question suivante
    $('#quizz-fa .quizz-custom-wrapper[data-question="question16"] #question-suivante').click(function(){
        $('#quizz-fa .quizz-custom-wrapper[data-question="question16"]').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question16"]').removeClass('active');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question17"]').show();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question17"]').addClass('active');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question17"] #question-suivante').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question17"] #confirmer-reponse').prop('disabled', true);
        $('#quizz-fa .quizz-dot-17').addClass('active');
    });


    // Q17, au clic du bouton de validation, vérifie la réponse et affiche le résultat
    $('#quizz-fa .quizz-custom-wrapper[data-question="question17"] #confirmer-reponse').click(function(){
        var reponse = $('#quizz-fa .quizz-question .active button').attr('data-reponse');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question17"] .reponse-a button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question17"] .reponse-b button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question17"] .reponse-c button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question17"] .reponse-d button').prop('disabled', true);
        if (reponse == "A") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question17"] .reponse-a').addClass('reponse-validee-ok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question17"] .quizz-question-reponse').append(q17Vrai);
        } else if (reponse == "B") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question17"] .reponse-b').addClass('reponse-validee-ok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question17"] .quizz-question-reponse').append(q17Vrai);
        } else if (reponse == "C") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question17"] .reponse-c').addClass('reponse-validee-ok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question17"] .quizz-question-reponse').append(q17Vrai);
        } else {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question17"] .reponse-d').addClass('reponse-validee-nok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question17"] .quizz-question-reponse').append(q17Faux);
        }
        $('#quizz-fa .quizz-custom-wrapper[data-question="question17"] #confirmer-reponse').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question17"] #question-suivante').show();
    });
    // Q17, passage à la question suivante
    $('#quizz-fa .quizz-custom-wrapper[data-question="question17"] #question-suivante').click(function(){
        $('#quizz-fa .quizz-custom-wrapper[data-question="question17"]').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question17"]').removeClass('active');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question18"]').show();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question18"]').addClass('active');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question18"] #question-suivante').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question18"] #confirmer-reponse').prop('disabled', true);
        $('#quizz-fa .quizz-dot-18').addClass('active');
    });


    // Q18, au clic du bouton de validation, vérifie la réponse et affiche le résultat
    $('#quizz-fa .quizz-custom-wrapper[data-question="question18"] #confirmer-reponse').click(function(){
        var reponse = $('#quizz-fa .quizz-question .active button').attr('data-reponse');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question18"] .reponse-a button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question18"] .reponse-b button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question18"] .reponse-c button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question18"] .reponse-d button').prop('disabled', true);
        if (reponse == "A") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question18"] .reponse-a').addClass('reponse-validee-nok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question18"] .quizz-question-reponse').append(q18Faux);
        } else if (reponse == "B") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question18"] .reponse-b').addClass('reponse-validee-nok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question18"] .quizz-question-reponse').append(q18Faux);
        } else if (reponse == "C") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question18"] .reponse-c').addClass('reponse-validee-ok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question18"] .quizz-question-reponse').append(q18Vrai);
        } else {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question18"] .reponse-d').addClass('reponse-validee-ok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question18"] .quizz-question-reponse').append(q18Vrai);
        }
        $('#quizz-fa .quizz-custom-wrapper[data-question="question18"] #confirmer-reponse').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question18"] #question-suivante').show();
    });
    // Q18, passage à la question suivante
    $('#quizz-fa .quizz-custom-wrapper[data-question="question18"] #question-suivante').click(function(){
        $('#quizz-fa .quizz-custom-wrapper[data-question="question18"]').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question18"]').removeClass('active');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question19"]').show();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question19"]').addClass('active');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question19"] #question-suivante').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question19"] #confirmer-reponse').prop('disabled', true);
        $('#quizz-fa .quizz-dot-19').addClass('active');
    });


    // Q19, au clic du bouton de validation, vérifie la réponse et affiche le résultat
    $('#quizz-fa .quizz-custom-wrapper[data-question="question19"] #confirmer-reponse').click(function(){
        var reponse = $('#quizz-fa .quizz-question .active button').attr('data-reponse');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question19"] .reponse-a button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question19"] .reponse-b button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question19"] .reponse-c button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question19"] .reponse-d button').prop('disabled', true);
        if (reponse == "A") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question19"] .reponse-a').addClass('reponse-validee-ok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question19"] .quizz-question-reponse').append(q19Vrai);
        } else if (reponse == "B") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question19"] .reponse-b').addClass('reponse-validee-ok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question19"] .quizz-question-reponse').append(q19Vrai);
        } else if (reponse == "C") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question19"] .reponse-c').addClass('reponse-validee-ok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question19"] .quizz-question-reponse').append(q19Vrai);
        } else {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question19"] .reponse-d').addClass('reponse-validee-ok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question19"] .quizz-question-reponse').append(q19Vrai);
        }
        $('#quizz-fa .quizz-custom-wrapper[data-question="question19"] #confirmer-reponse').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question19"] #question-suivante').show();
    });
    // Q19, passage à la question suivante
    $('#quizz-fa .quizz-custom-wrapper[data-question="question19"] #question-suivante').click(function(){
        $('#quizz-fa .quizz-custom-wrapper[data-question="question19"]').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question19"]').removeClass('active');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question20"]').show();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question20"]').addClass('active');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question20"] #retour-page-contenu').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question20"] #confirmer-reponse').prop('disabled', true);
        $('#quizz-fa .quizz-dot-20').addClass('active');
    });


    // Q20, au clic du bouton de validation, vérifie la réponse et affiche le résultat
    $('#quizz-fa .quizz-custom-wrapper[data-question="question20"] #confirmer-reponse').click(function(){
        var reponse = $('#quizz-fa .quizz-question .active button').attr('data-reponse');
        $('#quizz-fa .quizz-custom-wrapper[data-question="question20"] .reponse-a button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question20"] .reponse-b button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question20"] .reponse-c button').prop('disabled', true);
        $('#quizz-fa .quizz-custom-wrapper[data-question="question20"] .reponse-d button').prop('disabled', true);
        if (reponse == "A") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question20"] .reponse-a').addClass('reponse-validee-ok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question20"] .quizz-question-reponse').append(q20Vrai);
        } else if (reponse == "B") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question20"] .reponse-b').addClass('reponse-validee-nok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question20"] .quizz-question-reponse').append(q20Faux);
        } else if (reponse == "C") {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question20"] .reponse-c').addClass('reponse-validee-ok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question20"] .quizz-question-reponse').append(q20Vrai);
        } else {
            $('#quizz-fa .quizz-custom-wrapper[data-question="question20"] .reponse-d').addClass('reponse-validee-ok');
            $('#quizz-fa .quizz-custom-wrapper[data-question="question20"] .quizz-question-reponse').append(q20Vrai);
        }
        $('#quizz-fa .quizz-custom-wrapper[data-question="question20"] #confirmer-reponse').hide();
        $('#quizz-fa .quizz-custom-wrapper[data-question="question20"] #retour-page-contenu').show();
    });
    // Q20, retour au contenu de la page initiale et réinitialisation du quizz (Sans rechargement de page)
    $('#quizz-fa .quizz-custom-wrapper[data-question="question20"] #retour-page-contenu').click(function(){
        location.reload();
    });




})(jQuery, Drupal);