(function ($, Drupal) {

    'use strict';
  
    /* Quizz - Gestion des éléments principaux au GLOBAL */
    // État initial
    $('#quizz-fa').hide();
    $('#quizz-avc').hide();
    $('#quizz-phlebite').hide();
    $('#quizz-ep').hide();
    $('.quizz-custom-wrapper').hide();
    $('.quizz-custom-wrapper.active').show();
    $('.quizz-custom-wrapper.active #question-suivante').hide();
    $('.quizz-custom-wrapper.active #retour-page-contenu').hide();

    // Affichage au clic du formulaire, cache le contenu de la page
    $('.quizz-lien').click(function(){
        $('.contenu-page-left-titre').hide();
        $('.field--name-field-texte-comprendre').hide();
        $('.field--name-field-texte-pec').hide();
        $('.field--name-field-texte-suivi').hide();
        $('.sidebar-quizz').hide();
        $('#trucs-et-astuces').hide();
        $('#mon-memo').hide();
        $('#les-associations').hide();
        if ($(this).attr('href') == '#quizz-fa') {
            $('#quizz-fa').show();
        } else if ($(this).attr('href') == '#quizz-avc') {
            $('#quizz-avc').show();
        } else if ($(this).attr('href') == '#quizz-phlebite') {
            $('#quizz-phlebite').show();
        } else if ($(this).attr('href') == '#quizz-ep') {
            $('#quizz-ep').show();
        }
    });

    // Activation et désactivation au clic des réponses
    $('.quizz-custom-wrapper .reponse-a').click(function(){
        $('.quizz-custom-wrapper .reponse-b').removeClass('active');
        $('.quizz-custom-wrapper .reponse-c').removeClass('active');
        $('.quizz-custom-wrapper .reponse-d').removeClass('active');
        $('.quizz-custom-wrapper #confirmer-reponse').prop('disabled', false);
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $('.quizz-custom-wrapper #confirmer-reponse').prop('disabled', true);
        } else {
            $(this).addClass('active');
        }
    });
    $('.quizz-custom-wrapper .reponse-b').click(function(){
        $('.quizz-custom-wrapper .reponse-a').removeClass('active');
        $('.quizz-custom-wrapper .reponse-c').removeClass('active');
        $('.quizz-custom-wrapper .reponse-d').removeClass('active');
        $('.quizz-custom-wrapper #confirmer-reponse').prop('disabled', false);
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $('.quizz-custom-wrapper #confirmer-reponse').prop('disabled', true);
        } else {
            $(this).addClass('active');
        }
    });
    $('.quizz-custom-wrapper .reponse-c').click(function(){
        $('.quizz-custom-wrapper .reponse-a').removeClass('active');
        $('.quizz-custom-wrapper .reponse-b').removeClass('active');
        $('.quizz-custom-wrapper .reponse-d').removeClass('active');
        $('.quizz-custom-wrapper #confirmer-reponse').prop('disabled', false);
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $('.quizz-custom-wrapper #confirmer-reponse').prop('disabled', true);
        } else {
            $(this).addClass('active');
        }
    });
    $('.quizz-custom-wrapper .reponse-d').click(function(){
        $('.quizz-custom-wrapper .reponse-a').removeClass('active');
        $('.quizz-custom-wrapper .reponse-b').removeClass('active');
        $('.quizz-custom-wrapper .reponse-c').removeClass('active');
        $('.quizz-custom-wrapper #confirmer-reponse').prop('disabled', false);
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $('.quizz-custom-wrapper #confirmer-reponse').prop('disabled', true);
        } else {
            $(this).addClass('active');
        }
    });

    // Désactivation du bouton de validation si aucune réponse n'est saisie
    if ($('.quizz-question .reponse-a').hasClass('active') || $('.quizz-question .reponse-b').hasClass('active') || $('.quizz-question .reponse-c').hasClass('active') || $('.quizz-question .reponse-d').hasClass('active')) {
        $('#confirmer-reponse').prop('disabled', false);
    } else {
        $('#confirmer-reponse').prop('disabled', true);
    }

})(jQuery, Drupal);